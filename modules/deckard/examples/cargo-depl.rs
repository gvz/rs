extern crate deckard;
extern crate x;

fn main() -> std::io::Result<()> {
    use deckard::cui;
    use x::pool;

    let mut cui = cui::Cui::new(std::io::stdout(), deckard::themes::STD_STYLER);

    let h1 = cui
        .strings
        .whirl(pool::string_setter("Welcome to: Deckard"));
    let h1 = deckard::cui::block::H1(h1);
    cui.h1(&h1);

    let choose_an_option = cui
        .strings
        .whirl(pool::string_setter("Please choose an option: "));
    let choose_an_option = deckard::cui::animation::Scrolling(choose_an_option);
    cui.add_animation(&choose_an_option);

    cui.main()?;
    Ok(())
}
