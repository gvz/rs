use {
    std::{fmt, io, rc},
    x::pool,
};

pub trait TerminalColour {
    fn apply(
        &self,
        obj: &dyn fmt::Display,
        callb: &mut dyn FnMut(&dyn fmt::Display) -> io::Result<()>,
    ) -> io::Result<()>;
}

#[derive(Debug, Default, Clone, Copy)]
pub struct AnsiColour(pub u8);
impl TerminalColour for AnsiColour {
    fn apply(
        &self,
        o: &dyn fmt::Display,
        k: &mut dyn FnMut(&dyn fmt::Display) -> io::Result<()>,
    ) -> io::Result<()> {
        k(&format_args!("\x1b[38:5:{}m{}\x1b[m", self.0, o))
    }
}

#[derive(Debug, Default)]
pub struct Styler<H1: TerminalColour> {
    pub h1: H1,
}
pub trait IStyler {
    fn h1(&self) -> &dyn TerminalColour;
}

pub struct Cui<'block, O, H1>
where
    H1: TerminalColour,
{
    pub output: O,
    pub styler: Styler<H1>,
    pub blocks: Vec<&'block dyn Block>,
    pub strings: pool::Pool<String>,
}

pub trait Block {
    fn render(&self, styler: &dyn IStyler, output: &mut dyn io::Write) -> io::Result<()>;
}

impl<'b, O, H1> Cui<'b, O, H1>
where
    O: io::Write,
    H1: TerminalColour + Default,
{
    pub fn new(output: O, styler: Styler<H1>) -> Self {
        Self {
            output,
            styler,
            blocks: Default::default(),
            strings: Default::default(),
        }
    }
    pub fn h1(&mut self, h1: &'b block::H1) {
        self.blocks.push(h1);
    }

    pub fn add_animation(&mut self, anim: &'b animation::Scrolling) {
        todo!("add_animmation");
    }

    pub fn main(self) -> io::Result<Self> {
        let Self {
            mut output,
            styler,
            blocks,
            strings,
            ..
        } = self;

        for block in &blocks {
            block.render(&styler, &mut output)?;
        }

        let next = Self {
            output,
            styler,
            blocks,
            strings,
        };
        Ok(next)
    }
}

pub mod block {
    use super::*;
    pub struct H1(pub rc::Rc<String>);
}
pub mod animation {
    use super::*;
    pub struct Scrolling(pub rc::Rc<String>);
}

impl Block for block::H1 {
    fn render(&self, styler: &dyn IStyler, output: &mut dyn io::Write) -> io::Result<()> {
        todo!()
    }
}

impl<H1: TerminalColour> IStyler for Styler<H1> {
    fn h1(&self) -> &dyn TerminalColour {
        &self.h1
    }
}
