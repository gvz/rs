use super::cui;

pub struct Theme {
    h1: cui::AnsiColour,
}

pub type Styler = cui::Styler<cui::AnsiColour>;

impl Theme {
    pub const fn to_styler(self) -> Styler {
        let Self { h1, .. } = self;
        Styler { h1 }
    }
}

pub const STD: Theme = Theme {
    h1: cui::AnsiColour(57),
};

pub const STD_STYLER: Styler = STD.to_styler();
