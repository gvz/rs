use {
    super::{gl, glx, libgl},
    libgl::fty,
};

pub struct Portal {
    gl: gl::Context,
    vshsrc: String,
    buffer: glx::Buffer,
    prog: glx::Program,
}

impl Portal {
    pub fn set_camera<x, y, z, s, t, u>(&mut self, x:x,y:y,z:z,s:s,t:t,u:u) -> gl::Result<()>
    where
        x: Into<f32>,
        y: Into<f32>,
        z: Into<f32>,
        s: Into<f32>,
        t: Into<f32>,
        u: Into<f32>,
    {
        let x = x.into();
        let y = x.into();
        let z = x.into();
        let s = x.into();
        let t = x.into();
        let u = x.into();
        let loc = self.prog.uniloc("camera")?;
        let gl = &self.gl;

        gl.uniform_matrix3fv_with_f32_array(
            loc,
            false,
            &[
                x, y, z,
                s, t, u,
                0., 0., 0.,
            ],
        );
        Ok(())
    }

    pub fn draw_std(&mut self, data: &[f32]) -> gl::Result<()> {
        self.buffer.bind_to(glx::buf::Target::Array)?;
        self.buffer.load(data)?;

        let gl = &self.gl;
        {
            let index = self.prog.attrib("position")?;
            let vertex_size = 3;
            gl.vertex_attrib_pointer_with_i32(index, vertex_size, gl::GL::FLOAT, false, 0, 0);
            gl.enable_vertex_attrib_array(index);
        }

        gl.clear_color(0.3, 0.3, 0.3, 1.0);
        gl.clear_depth(1.0);
        gl.enable(gl::GL::DEPTH_TEST);
        gl.depth_func(gl::GL::LEQUAL);

        gl.clear(gl::GL::COLOR_BUFFER_BIT | gl::GL::DEPTH_BUFFER_BIT);
        gl.draw_arrays(gl::GL::LINE_STRIP, 0, (data.len() / 3) as i32);
        Ok(())
    }

    pub fn new(mut gl: gl::Context) -> gl::Result<Self> {
        let mut shaders = glx::shade::Bind(&mut gl);
        let vsh = shaders.compile(glx::shade::Type::Vertex, fty::VSH)?;
        let fsh = shaders.compile(glx::shade::Type::Fragment, fty::FSH)?;

        let mut prog = glx::prog::Bind(&mut gl);
        let prog = prog.link(vec![vsh, fsh])?;

        gl.use_program(Some(&prog));

        glx::buf::new(&mut gl)?.bind_to(glx::buf::Target::Element)?;

        let buffer: glx::Buffer = glx::buf::new(&mut gl)?;
        let r = Self {
            gl,
            vshsrc: Default::default(),
            buffer,
            prog,
        };

        Ok(r)
    }

    pub fn set_vshsrc<S: Into<String>>(&mut self, s: S) {
        self.vshsrc = s.into();
    }

    pub fn init(&mut self) -> gl::Result<()> {
        //self.draw_std(&fty::cube())?;
        self.draw_std(&fty::cage())?;
        Ok(())
    }
}
