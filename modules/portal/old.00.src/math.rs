use std::{
    borrow::{Borrow, BorrowMut},
    ops::{Add, AddAssign, Deref, Mul},
};

pub trait Vec3<T>: BorrowMut<[T; 3]> + Into<[T; 3]> {
    fn as_ref(&self) -> &[T; 3] {
        self.borrow()
    }
    fn x(&self) -> &T {
        &self.as_ref()[0]
    }
    fn y(&self) -> &T {
        &self.as_ref()[1]
    }
    fn z(&self) -> &T {
        &self.as_ref()[2]
    }

    fn clone(&self) -> V3<T>
    where
        T: Clone,
    {
        V3(self.copy())
    }

    fn copy(&self) -> [T; 3]
    where
        T: Clone,
    {
        let [x, y, z] = self.as_ref();
        [x.clone(), y.clone(), z.clone()]
    }

    fn write3<U>(&self, trg: &mut [U])
    where
        T: Clone + Into<U>,
    {
        let [x, y, z] = self.copy();
        trg[0] = x.into();
        trg[1] = y.into();
        trg[2] = z.into();
    }

    fn map0<F, U>(self, mut f: F) -> V3<U>
    where
        Self: Sized,
        F: FnMut(T) -> U,
    {
        self.map(|_, x| f(x))
    }
    fn map<F, U>(self, mut f: F) -> V3<U>
    where
        Self: Sized,
        F: FnMut(usize, T) -> U,
    {
        let [x, y, z] = self.into();
        V3([f(0, x), f(1, y), f(2, z)])
    }
    fn map_inplace<F>(&mut self, mut f: F)
    where
        F: FnMut(usize, &mut T),
    {
        let [x, y, z] = self.borrow_mut();
        f(0, x);
        f(1, y);
        f(2, z);
    }
}
impl<T, U> AddAssign<V3<U>> for V3<T>
where
    T: AddAssign<U>,
{
    fn add_assign(&mut self, other: V3<U>) {
        let [a, b, c] = other.0;
        self.0[0] += a;
        self.0[1] += b;
        self.0[2] += c;
    }
}
impl<T, U> Mul<U> for V3<T>
where
    T: Into<U>,
    U: Mul + Clone,
{
    type Output = V3<U::Output>;
    fn mul(self, other: U) -> Self::Output {
        self.map(move |_, v| v.into() * other.clone())
    }
}

#[derive(Debug, PartialEq, Eq)]
pub struct V3<T>(pub [T; 3]);
impl<T> Vec3<T> for V3<T> {}
impl<T> Borrow<[T; 3]> for V3<T> {
    fn borrow(&self) -> &[T; 3] {
        &self.0
    }
}
impl<T> BorrowMut<[T; 3]> for V3<T> {
    fn borrow_mut(&mut self) -> &mut [T; 3] {
        &mut self.0
    }
}
impl<T> Into<[T; 3]> for V3<T> {
    fn into(self) -> [T; 3] {
        self.0
    }
}
