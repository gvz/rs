use crate::math::{Vec3 as IVec3, V3};
pub type Vec3 = V3<i8>;

mod p {
    use super::*;
    pub const RIGHT: Vec3 = V3([1, 0, 0]);
    pub const LEFT: Vec3 = V3([-1, 0, 0]);
    pub const UP: Vec3 = V3([0, 1, 0]);
    pub const DOWN: Vec3 = V3([0, -1, 0]);
    pub const FRONT: Vec3 = V3([0, 0, 1]);
    pub const BACK: Vec3 = V3([0, 0, -1]);

    pub const A: Vec3 = V3([-1, -1, -1]);
    pub const B: Vec3 = V3([1, -1, -1]);
    pub const C: Vec3 = V3([-1, 1, -1]);
    pub const D: Vec3 = V3([1, 1, -1]);
    pub const E: Vec3 = V3([-1, 1, 1]);
    pub const F: Vec3 = V3([1, 1, 1]);
    pub const G: Vec3 = V3([-1, -1, 1]);
    pub const H: Vec3 = V3([1, -1, 1]);
    //
    //          C-------D
    //         /|      /|
    //        / |     / |
    //       E--+----F  |
    //       |  A----+--B
    //       | /     | /
    //       |/      |/
    //       G-------H
    //
}

pub fn turn_right(d: &mut Vec3) {
    *d = match *d {
        p::RIGHT => p::FRONT,
        p::FRONT => p::LEFT,
        p::LEFT => p::BACK,
        p::BACK => p::RIGHT,
        _ => panic!("Unknown direction: {:?}", d),
    };
}

pub const CUBE_LEN: usize = (2 * 4) * 2 + 1;
pub const CUBE_BYTES: usize = CUBE_LEN * 3;
pub fn cube() -> [f32; CUBE_BYTES] {
    let mut r = [0_f32; CUBE_BYTES];
    let mut i = 0;
    use p::*;
    for p in &[A, B, C, D, E, F, G, H, H, G, A, E, C, F, D, H, B] {
        let e = i + 3;
        p.write3(&mut r[i..e]);
        i += 3;
    }
    r
}

pub const CAGE_LEN: usize = (20 * 4) * 21;
pub const CAGE_BYTES: usize = CAGE_LEN * 3;
pub fn cage() -> [f32; CAGE_BYTES] {
    let mut r = [0_f32; CAGE_BYTES];
    let mut i = 0;
    let mut push = |p: &V3<f32>| {
        p.write3(&mut r[i..(i + 3)]);
        i += 3;
    };

    use p::*;

    let mut d: Vec3 = p::RIGHT;
    let mut t: V3<f32> = p::A.map0(<_>::into);
    for f in 0..=20 {
        for r in 0..4 {
            for l in 0..20 {
                push(&t);
                t += d.clone() * 0.1_f32;
            }
            turn_right(&mut d);
        }
        t += p::UP * 0.1_f32;
    }

    r
}
#[test]
fn cage_no_loop() {
    cage();
}

pub const VSH: &str = r#"
        #define M_PI 3.1415926535897932384626433832795

        mat4 identity() {
            return mat4(1.0);
        }

        mat4 translate(vec3 d) {
            return mat4(
                1.0, 0.0, 0.0, 0.0,
                0.0, 1.0, 0.0, 0.0,
                0.0, 0.0, 1.0, 0.0,
                d.x, d.y, d.z, 1.0
            );
        }

        mat4 scale(vec3 d) {
            return mat4(
                d.x, 0.0, 0.0, 0.0,
                0.0, d.y, 0.0, 0.0,
                0.0, 0.0, d.z, 0.0,
                0.0, 0.0, 0.0, 1.0
            );
        }

        mat4 rotateX(float r) {
            return mat4(
                1.0,     0.0,     0.0, 0.0,
                0.0,  cos(r), -sin(r), 0.0,
                0.0,  sin(r),  cos(r), 0.0,
                0.0,     0.0,     0.0, 1.0
            );
        }

        mat4 rotateY(float r) {
            return mat4(
                 cos(r), 0.0,  sin(r), 0.0,
                    0.0, 1.0,     0.0, 0.0,
                -sin(r), 0.0,  cos(r), 0.0,
                    0.0, 0.0,     0.0, 1.0
            );
        }

        mat4 rotateZ(float r) {
            return mat4(
                 cos(r), -sin(r), 0.0, 0.0,
                 sin(r),  cos(r), 0.0, 0.0,
                    0.0,     0.0, 1.0, 0.0,
                    0.0,     0.0, 0.0, 1.0
            );
        }

        /**
         *
         * fieldOfviewInRadians
         *      An angle, given in radians, indicating how much of the scene is visible
         *      to the viewer at once. The larger the number is, the more is visible by
         *      the camera. The geometry at the edges becomes more and more distorted,
         *      equivalent to a wide angle lens. When the field of view is larger, the
         *      objects typically get smaller. When the field of view is smaller, then
         *      the camera can see less and less in the scene. The objects are distorted
         *      much less by perspective and objects seem much closer to the camera.
         *
         * aspectRatio
         *      The scene's aspect ratio, which is equivalent to its width divided by
         *      its height. In these examples, that's the window's width divided by
         *      the window height. The introduction of this parameter finally solves
         *      the problem wherein the model gets warped as the canvas is resized
         *      and reshaped.
         *
         * nearClippingPlaneDistance
         *      A positive number indicating the distance into the screen to a plane
         *      which is perpendicular to the floor, nearer than which everything gets
         *      clipped away. This is mapped to -1 in clip space, and should not be set
         *      to 0.
         *
         * farClippingPlaneDistance
         *      A positive number indicating the distance to the plane beyond which
         *      geometry is clipped away. This is mapped to 1 in clip space. This value
         *      should be kept reasonably close to the distance of the geometry in order
         *      to avoid precision errors creeping in while rendering.
         *
         * In the latest version of the box demo, the computeSimpleProjectionMatrix()
         * method has been replaced with the computePerspectiveMatrix() method.
         *
         *      CubeDemo.prototype.computePerspectiveMatrix = function() {
         *        var fieldOfViewInRadians = Math.PI * 0.5;
         *        var aspectRatio = window.innerWidth / window.innerHeight;
         *        var nearClippingPlaneDistance = 1;
         *        var farClippingPlaneDistance = 50;
         *
         *        this.transforms.projection = MDN.perspectiveMatrix(
         *          fieldOfViewInRadians,
         *          aspectRatio,
         *          nearClippingPlaneDistance,
         *          farClippingPlaneDistance
         *        );
         *      };
         *
         * The shader code is identical to the previous example:
         *
         *      gl_Position = projection * model * vec4(position, 1.0);
         *
         * Additionally (not shown), the position and scale matrices of the model have
         * been changed to take it out of clip space and into the larger coordinate system.
         *
         */
        mat4 perspective(
            float fieldOfViewInRadians,
            float aspectRatio,
            float near,
            float far
        ) {
            float f = 1.0 / tan(fieldOfViewInRadians / 2.0);
            float rangeInv = 1.0 / (near - far);

            return mat4(
                 f / aspectRatio,    0.0,                         0.0,   0.0,
                             0.0,      f,                         0.0,   0.0,
                             0.0,    0.0,     (near + far) * rangeInv,  -1.0,
                             0.0,    0.0, near * far * rangeInv * 2.0,   0.0
            );
        }

        attribute vec3 position;
        uniform mat3 camera;
        varying vec3 color;

        void main() {
            mat4 model =
            /*
                rotateY(M_PI / 5.0) *
                rotateX(M_PI / 6.0) *
            */
                scale(vec3(0.5)) *
                mat4(1.0)
                ;
            mat4 view =
                translate(camera[0] * -1.0) +
                mat4(1.0)
                ;
            mat4 projection =
                mat4(1.0)
                ;

            mat3 unused = camera;

            gl_Position =
                projection *
                view *
                model *
                vec4(position, 1.0);
            gl_PointSize = 20.0;

            color = (position + 1.0) / 2.0;
        }
    "#;
pub const FSH: &str = r#"
        varying lowp vec3 color;

        void main() {
            gl_FragColor = vec4(color, 1.0);
        }
    "#;
