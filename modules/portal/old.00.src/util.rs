pub fn try_opt<T, EF, E>(opt: Option<T>, err: EF) -> Result<T, E>
where
    EF: FnOnce() -> E,
{
    if let Some(t) = opt {
        Ok(t)
    } else {
        Err(err())
    }
}
