// Module global usages
use {
    super::{gl, util},
    std::{
        borrow::{Borrow, BorrowMut},
        collections::HashMap as Map,
        fmt,
        hash::Hash,
        marker::PhantomData as __,
    },
};

// Public Interface: Core
pub mod adapt;
pub mod buf;
pub mod cast;
pub mod param;
pub use {
    adapt::{Adapt, Adaptation},
    buf::Buffer,
    cast::{typ, Caster},
    param::{Param, Parameter},
};

// Public Interface: Modular
pub mod prog;
pub type Program = Adapt<gl::Program>;

pub mod shade;
