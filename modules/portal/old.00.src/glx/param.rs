use super::*;

pub trait Parameter {
    type Subject;
    type Type: gl::Type;
    fn desc() -> &'static str;
    fn get(ctx: &gl::Context, subj: &Self::Subject) -> typ::GlValue<Self::Type>;
}
pub struct Param<P: Parameter + ?Sized>(__<*const (P,)>);

impl<P: Parameter + ?Sized> Param<P> {
    pub fn new() -> Self {
        Self(__)
    }

    pub fn param(&self, ctx: &gl::Context, subj: &P::Subject) -> gl::Result<typ::Native<P::Type>> {
        let gl = P::get(ctx, subj);
        let native = Caster::<P::Type>::cast(P::desc(), gl)?;
        Ok(native)
    }
}

macro_rules! param {
    ($name:ident, $subj:ident, $typ:ident, $getter:ident, $gl_name:ident) => {
        pub enum $name {}
        impl Parameter for $name {
            type Subject = gl::$subj;
            type Type = gl::types::$typ;
            fn desc() -> &'static str {
                stringify!($gl_name)
            }
            fn get(ctx: &gl::Context, subj: &Self::Subject) -> typ::GlValue<Self::Type> {
                ctx.$getter(subj, gl::GL::$gl_name)
            }
        }
    };
}

param![
    CompileStatus,
    Shader,
    Bool,
    get_shader_parameter,
    COMPILE_STATUS
];
param![
    LinkStatus,
    Program,
    Bool,
    get_program_parameter,
    LINK_STATUS
];
param![
    ActiveAttributes,
    Program,
    Int,
    get_program_parameter,
    ACTIVE_ATTRIBUTES
];

param![
    ActiveUniforms,
    Program,
    Int,
    get_program_parameter,
    ACTIVE_UNIFORMS
];
