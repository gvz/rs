pub trait Adaptation {
    type Data;
}
pub struct Adapt<T: Adaptation>(pub(super) T, pub(super) T::Data);

impl<T> std::ops::Deref for Adapt<T>
where
    T: Adaptation,
{
    type Target = T;
    fn deref(&self) -> &T {
        &self.0
    }
}
impl<T> std::ops::DerefMut for Adapt<T>
where
    T: Adaptation,
{
    fn deref_mut(&mut self) -> &mut T {
        &mut self.0
    }
}
