use super::*;

#[derive(Default)]
pub struct ProgramData {
    attrs: Map<String, u32>,
    uniforms: Map<String, (u32, gl::UniformLocation)>,
}

impl Adaptation for gl::Program {
    type Data = ProgramData;
}

impl super::Program {
    pub fn attrib<Q: ?Sized>(&self, name: &Q) -> gl::Result<u32>
    where
        String: Borrow<Q>,
        Q: Hash + Eq + fmt::Display,
    {
        let v: Option<&u32> = self.1.attrs.get(name);

        use util::try_opt;
        let err = || gl::Error::AttribNotExist(name.to_string());

        let v = try_opt(v, err)?;

        Ok(v.clone())
    }

    //pub fn unifm<Q: ?Sized>(&self, name: &Q) -> gl::Result<u32>
    //where
    //    String: Borrow<Q>,
    //    Q: Hash + Eq + fmt::Display,
    //{
    //    let v: Option<&_> = self.1.uniforms.get(name);

    //    use util::try_opt;
    //    let err = || gl::Error::UniformNotExist(name.to_string());

    //    let v = try_opt(v, err)?;

    //    Ok(v.0)
    //}

    pub fn uniloc<Q: ?Sized>(&self, name: &Q) -> gl::Result<Option<&gl::UniformLocation>>
    where
        String: Borrow<Q>,
        Q: Hash + Eq + fmt::Display,
    {
        let v: Option<&_> = self.1.uniforms.get(name);

        use util::try_opt;
        let err = || gl::Error::UniformNotExist(name.to_string());

        let v = try_opt(v, err)?;

        Ok(Some(&v.1))
    }
}

pub fn get_program_data(ctx: &gl::Context, program: &gl::Program) -> gl::Result<ProgramData> {
    let mut result = ProgramData::default();

    let num_attributes = Param::<param::ActiveAttributes>::new().param(ctx, program)?;
    for i in 0..num_attributes {
        let err = || gl::Error::AttribNotFound(i);
        let info_opt = ctx.get_active_attrib(&program, i);
        let info = util::try_opt(info_opt, err)?;
        let name = info.name();
        result.attrs.insert(name, i);
    }

    let num_uniforms = Param::<param::ActiveUniforms>::new().param(ctx, program)?;
    for i in 0..num_uniforms {
        let err = |name| || gl::Error::UniformNotFound(i, name);
        let info_opt = ctx.get_active_uniform(&program, i);
        let info = util::try_opt(info_opt, err(None))?;
        let name = info.name();

        let location = ctx.get_uniform_location(&program, &name);
        let location = util::try_opt(location, err(Some(name.clone())))?;

        result.uniforms.insert(name, (i, location));
    }

    Ok(result)
}

pub struct Bind<Ctx>(pub Ctx);
impl<Ctx> Bind<Ctx>
where
    Ctx: BorrowMut<gl::Context>,
{
    pub fn link<Shaders>(&mut self, shaders: Shaders) -> gl::Result<Program>
    where
        Shaders: IntoIterator,
        Shaders::Item: BorrowMut<gl::Shader>,
    {
        let ctx: &mut gl::Context = self.0.borrow_mut();
        let mut program: gl::Program = ctx.create_program()?;
        for mut shader in shaders {
            ctx.attach_shader(&program, shader.borrow_mut());
        }
        ctx.link_program(&mut program);

        let success: bool = Param::<param::LinkStatus>::new().param(ctx, &program)?;
        let ctx: &gl::Context = ctx;

        if success {
            let program_data = get_program_data(ctx, &program)?;
            Ok(Adapt(program, program_data))
        } else {
            let log = if let Some(log) = ctx.get_program_info_log(&program) {
                log
            } else {
                "No program info log".to_owned()
            };
            Err(gl::Error::ProgramLink(log))
        }
    }
}
