use {super::*, gl::Context, gl::Result, gl::GL};

#[derive(Debug)]
pub enum Type {
    Vertex,
    Fragment,
}
impl Type {
    pub fn to_gl(&self) -> u32 {
        match self {
            Self::Vertex => GL::VERTEX_SHADER,
            Self::Fragment => GL::FRAGMENT_SHADER,
        }
    }
}

pub struct Bind<Ctx>(pub Ctx);
impl<Ctx> Bind<Ctx>
where
    Ctx: BorrowMut<gl::Context>,
{
    pub fn compile<Src>(&mut self, t: Type, source: Src) -> Result<gl::Shader>
    where
        Src: AsRef<str>,
    {
        let ctx: &mut Context = self.0.borrow_mut();
        let shader = ctx.create_shader(t.to_gl())?;
        ctx.shader_source(&shader, source.as_ref());
        ctx.compile_shader(&shader);

        let ctx: &Context = ctx;
        let success: bool = Param::<param::CompileStatus>::new().param(ctx, &shader)?;
        if success {
            Ok(shader)
        } else {
            let log_opt = ctx.get_shader_info_log(&shader);
            let log: &str = if let Some(log) = log_opt.as_ref() {
                log
            } else {
                "No log info"
            };
            let log = format!("[{:?}] {}", t, log);
            Err(gl::Error::ShaderCompile(log))
        }
    }
}
