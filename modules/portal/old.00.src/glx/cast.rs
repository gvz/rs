use super::*;

pub mod typ {
    use super::*;

    pub type GlValue<T> = <T as gl::Type>::Gl;
    pub type Native<T> = <T as gl::Type>::Native;
}

pub struct Caster<T: ?Sized>(__<*const (T,)>);
impl<T: ?Sized> Caster<T>
where
    T: gl::Type,
{
    pub fn cast(msg: &str, gl_value: typ::GlValue<T>) -> gl::Result<typ::Native<T>> {
        <T as gl::Type>::cast2(msg, gl_value)
    }
}
