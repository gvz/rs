use super::*;

pub struct Buffer {
    ctx: gl::Context,
    gl_buffer: gl::Buffer,
    target: Option<Target>,
}

pub enum Target {
    Array,
    Element,
}

impl Target {
    pub fn to_gl(&self) -> u32 {
        match self {
            Target::Array => gl::GL::ARRAY_BUFFER,
            Target::Element => gl::GL::ELEMENT_ARRAY_BUFFER,
        }
    }
}

pub fn new(ctx: &mut gl::Context) -> gl::Result<Buffer> {
    Ok(Buffer {
        ctx: ctx.clone(),
        gl_buffer: ctx.create_buffer()?,
        target: None,
    })
}

impl Buffer {
    pub fn bind_to(&mut self, target: Target) -> gl::Result<()> {
        if let Some(_) = &self.target {
            return Err(gl::Error::BufferRebind);
        }

        self.ctx.bind_buffer(target.to_gl(), Some(&self.gl_buffer));
        self.target = Some(target);

        Ok(())
    }

    pub fn load(&mut self, data: &[f32]) -> gl::Result<()> {
        if let Some(target) = &self.target {
            self.ctx.buffer_data(target.to_gl(), data);
            return Ok(());
        }

        Err(gl::Error::BufferNotBound)
    }
}
