use super::*;

pub trait Provider: Sized {
    type Context: Context<Self>;
    type Program: Program<Self>;
    type Shader: Shader<Self>;
    type Buffer: Buffer<Self>;

    type NativeError;

    fn open(self) -> Result<Self::Context>;
    fn to_native_error(&self, error: Error) -> Self::NativeError;
}
pub trait Context<P: ?Sized + Provider> {}
pub trait Program<P: ?Sized + Provider> {}
pub trait Shader<P: ?Sized + Provider> {}
pub trait Buffer<P: ?Sized + Provider> {}

pub fn provider<P, F, R>(provider: P, f: F) -> R
where
    P: Provider,
    F: FnOnce(P) -> R,
{
    f(provider)
}
