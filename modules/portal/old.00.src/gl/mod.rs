mod error;
mod icheck;

pub use error::{Error, Result};

#[cfg(feature = "webgl")]
pub mod webgl_adapter;
#[cfg(feature = "webgl")]
pub use webgl_adapter as adapter;

pub trait Type {
    type Gl;
    type Native;
    fn cast2(msg: &str, v: Self::Gl) -> Result<Self::Native>;
}
pub mod types {
    pub enum Bool {}
    pub enum Int {}
}

pub use adapter::{not_unused, Buffer, Context, Program, Provider, Shader, UniformLocation, GL};
