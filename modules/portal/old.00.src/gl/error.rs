pub type Result<T> = std::result::Result<T, Error>;

#[derive(Debug)]
pub enum Error {
    //NoProvider,
    NoContext(String),
    ShaderCompile(String),
    ProgramLink(String),
    BufferRebind,
    BufferNotBound,
    AttribNotFound(u32),
    AttribNotExist(String),
    UniformNotFound(u32, Option<String>),
    UniformNotExist(String),
    Native(super::adapter::NativeError),
}

impl From<super::adapter::NativeError> for Error {
    fn from(err: super::adapter::NativeError) -> Self {
        Self::Native(err)
    }
}
