use {
    super::{
        icheck::{self, Provider as Prov},
        types, Error, Result, Type,
    },
    wasm_bindgen::prelude::*,
};
mod _gl_env;
pub mod js {
    pub use {
        js_sys::{Array, Number, Reflect},
        wasm_bindgen::{JsCast as Cast, JsValue as Value},
    };
}
mod dom {
    pub use web_sys::{Document, Window};
}
mod html {
    pub use web_sys::HtmlCanvasElement as Canvas;
}
mod webgl {
    pub use super::js::Value;
    pub use web_sys::{
        WebGlBuffer as Buffer, WebGlProgram as Program, WebGlRenderingContext as Context,
        WebGlShader as Shader,
    };
}

const CANVAS_ID: &str = "canvas";

pub mod not_unused {
    pub use super::_gl_env::*;
}

pub fn provider(canvas_id: String) -> Result<Provider> {
    Ok(Provider { canvas_id })
}

pub struct Provider {
    canvas_id: String,
}

pub type NativeError = js::Value;
fn to_native_error(error: Error) -> NativeError {
    format!("{:?}", error).into()
}

pub use web_sys::WebGlRenderingContext as GL;

impl Prov for Provider {
    type Context = Context;
    type Program = Program;
    type Shader = Shader;
    type Buffer = Buffer;
    type NativeError = NativeError;

    fn open(self) -> Result<Self::Context> {
        let ctx = open(&self.canvas_id).map_err(|err| Error::NoContext(format!("{:?}", err)))?;
        Ok(Adapt(ctx))
    }

    fn to_native_error(&self, error: Error) -> Self::NativeError {
        to_native_error(error)
    }
}

#[derive(Debug)]
pub struct Adapt<T>(T);
pub type Context = Adapt<webgl::Context>;
pub type Program = Adapt<webgl::Program>;
pub type Shader = Adapt<webgl::Shader>;
pub type Buffer = Adapt<webgl::Buffer>;
pub use web_sys::WebGlUniformLocation as UniformLocation;

impl<P: Prov> icheck::Context<P> for Context {}
impl<P: Prov> icheck::Program<P> for Program {}
impl<P: Prov> icheck::Shader<P> for Shader {}
impl<P: Prov> icheck::Buffer<P> for Buffer {}

#[wasm_bindgen(start)]
pub fn wasm_main() -> std::result::Result<(), js::Value> {
    #[cfg(feature = "console_error_panic_hook")]
    console_error_panic_hook::set_once();

    const APPLICATION_ERROR_WRAPPER: fn() -> Result<()> = || {
        let ctx = icheck::provider(provider(CANVAS_ID.to_owned())?, |p| {
            icheck::Provider::open(p)
        })?;

        let mut portal = crate::portal::Portal::new(ctx)?;
        portal.init()?;

        Ok(())
    };
    APPLICATION_ERROR_WRAPPER().map_err(|err| format!("Application error: {:?}", err))?;

    Ok(())
}

fn get_opt<T>(msg: &str, opt: Option<T>) -> Result<T> {
    if let Some(obj) = opt {
        Ok(obj)
    } else {
        Err(Error::NoContext(msg.to_owned()))
    }
}

trait Cast: js::Cast {
    fn dyn_cast<U: js::Cast>(self, into: &str) -> Result<U> {
        self.dyn_into()
            .map_err(|_| Error::NoContext(format!("Casting into {}", into)))
    }

    fn u32_cast(self) -> Result<u32> {
        let num: js::Number = self.dyn_cast("number")?;
        let val: f64 = num.value_of();
        let error = |msg| Err(Error::Native(format!("{}: {}", msg, val).into()));
        if js::Number::is_integer(&num) {
            if val < 0_f64 {
                error("not unsigned")
            } else if val > u32::MAX.into() {
                error("too large for u32")
            } else {
                Ok(val as u32)
            }
        } else {
            error("not an integer")
        }
    }
}
impl<J: js::Cast> Cast for J {}

fn window() -> Result<dom::Window> {
    get_opt("No window", web_sys::window())
}

fn document() -> Result<dom::Document> {
    get_opt("No document", window()?.document())
}

fn canvas(id: &str) -> Result<html::Canvas> {
    let canvas = get_opt("No canvas", document()?.get_element_by_id(id))?;
    Ok(canvas.dyn_cast("canvas")?)
}

fn context(id: &str) -> Result<webgl::Context> {
    let canvas = canvas(id)?;
    let context = canvas
        .get_context("webgl")
        .map_err(|e| Error::NoContext(format!("{:?}", e)))?;
    let context = get_opt("No webgl context", context)?;
    let context = context.dyn_cast("WebGlRenderingContext")?;
    Ok(context)
}

fn open(canvas_id: &str) -> Result<webgl::Context> {
    Ok(context(canvas_id)?)
}

impl<T> std::ops::Deref for Adapt<T> {
    type Target = T;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<T> std::ops::DerefMut for Adapt<T> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}

impl Context {
    pub fn create_shader(&mut self, typ: u32) -> Result<Shader> {
        let Self(ctx) = self;
        let shader: Option<webgl::Shader> = ctx.create_shader(typ);
        let shader = get_opt("Create shader", shader)?;
        let shader = Adapt(shader);
        Ok(shader)
    }

    pub fn create_program(&mut self) -> Result<Program> {
        let Self(ctx) = self;
        let program: Option<webgl::Program> = ctx.create_program();
        let program = get_opt("Create program", program)?;
        let program = Adapt(program);
        Ok(program)
    }

    pub fn create_buffer(&mut self) -> Result<Buffer> {
        let Self(ctx) = self;
        let buffer: Option<webgl::Buffer> = ctx.create_buffer();
        let buffer = get_opt("Create buffer", buffer)?;
        let buffer = Adapt(buffer);
        Ok(buffer)
    }

    pub fn clone(&self) -> Self {
        Adapt(webgl::Context::clone(self))
    }

    pub fn buffer_data(&mut self, target: u32, data: &[f32]) {
        let Self(ctx) = self;

        // Note that `Float32Array::view` is somewhat dangerous (hence the
        // `unsafe`!). This is creating a raw view into our module's
        // `WebAssembly.Memory` buffer, but if we allocate more pages for ourself
        // (aka do a memory allocation in Rust) it'll cause the buffer to change,
        // causing the `Float32Array` to be invalid.
        //
        // As a result, after `Float32Array::view` we have to be very careful not to
        // do any memory allocations before it's dropped.
        unsafe {
            let vert_array = js_sys::Float32Array::view(data);
            ctx.buffer_data_with_array_buffer_view(target, &vert_array, GL::STATIC_DRAW);
        }
    }
}

impl Type for types::Bool {
    type Gl = js::Value;
    type Native = bool;
    fn cast2(msg: &str, v: Self::Gl) -> Result<Self::Native> {
        Ok(get_opt(msg, v.as_bool())?)
    }
}

impl Type for types::Int {
    type Gl = js::Value;
    type Native = u32;
    fn cast2(msg: &str, v: Self::Gl) -> Result<Self::Native> {
        let v = v.as_f64();
        let v: f64 = get_opt(msg, v)?;
        let v: u32 = v as u32;
        Ok(v)
    }
}
