use super::*;

use web_sys::{console, Document, HtmlTextAreaElement as TextArea};
mod wasm {
    pub use wasm_bindgen::prelude::Closure;
}

pub type R<T> = std::result::Result<T, js::Value>;

trait OrError {
    type T;
    fn or_error(self, msg: &str) -> R<Self::T>;
}
impl<T> OrError for Result<T> {
    type T = T;
    fn or_error(self, _: &str) -> R<T> {
        self.map_err(to_native_error)
    }
}
impl<T> OrError for Option<T> {
    type T = T;
    fn or_error(self, msg: &str) -> R<T> {
        if let Some(v) = self {
            return Ok(v);
        }
        Err(msg.into())
    }
}

pub type EventHandler = wasm::Closure<dyn Fn(&js::Value)>;
#[wasm_bindgen]
pub struct Dbject {
    vshedit: TextArea,
    on_vshedit_keydown: EventHandler,
}

#[wasm_bindgen]
impl Dbject {
    #[wasm_bindgen(constructor)]
    pub fn new() -> R<Dbject> {
        let doc: Document = document()
            .or_error("get document")?;

        let get_ta = |id: &str| -> R<TextArea> {
            let ta: TextArea = doc
                .get_element_by_id(id)
                .or_error("get textarea")?
                .dyn_cast("textarea")
                .or_error("cast textarea to textarea")?;
            Ok(ta)
        };

        let vshedit = get_ta("vshedit")?;
        let x = get_ta("camera_x")?;
        let y = get_ta("camera_y")?;
        let z = get_ta("camera_z")?;
        let s = get_ta("camera_s")?;
        let t = get_ta("camera_t")?;
        let u = get_ta("camera_u")?;

        let on_vshedit_keydown: EventHandler = {
            let vshedit = vshedit.clone();
            wasm::Closure::wrap(Box::new(move |_event| {
                let value = vshedit.value();
                console::log_2(
                    &"In theory would be setting vertex shader source to".into(),
                    &value.into(),
                );
            }))
        };
        vshedit.set_onkeydown(Some(
            js::Cast::dyn_ref(on_vshedit_keydown.as_ref())
                .or_error("Closure is not a JsFunction")?,
        ));

        Ok(Self {
            vshedit,
            on_vshedit_keydown,
        })
    }
}
