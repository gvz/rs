use super::*;

#[derive(Clone)]
pub struct Logger {}

impl entities::Log for Logger {
    fn info<F: fmt::Display>(&self, f: F) {
        log1(format!("{}", f).as_str())
    }
}
