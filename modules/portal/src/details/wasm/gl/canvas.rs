use {
    super::{super::log1, Context as GL},
    crate::entities,
};

#[derive(Clone)]
pub struct Canvas {
    ctx: GL,
}

impl Canvas {
    pub fn new(ctx: GL) -> Self {
        Self { ctx }
    }
}

impl entities::Canvas for Canvas {
    fn draw(&self) {
        log1("wasm::gl::Canvas: LOLOLOL");
        self.ctx.clear(GL::COLOR_BUFFER_BIT);
    }

    fn set_color(&mut self, color: entities::Color) {
        let Self { ctx, .. } = self;
        let [r, g, b] = color.take();
        ctx.clear_color(r, g, b, 1.0);
    }
}
