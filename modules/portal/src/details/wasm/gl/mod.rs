pub use {canvas::Canvas, web_sys::WebGlRenderingContext as Context};

use super::{html, Result, Show};
mod canvas;

Show! { @as_self Context }

pub struct Handle {
    ctx: Context,
}

impl Handle {
    pub fn new(html: &html::Handle) -> Result<Self> {
        todo!()
    }
}
