use super::*;
pub use web_sys::{window, Document, Element, HtmlCanvasElement as Canvas, Window};
Show! { @as_self Window }
Show! { @as_self Document }
Show! { @as_self Element }
Show! { @as_self Canvas }

pub struct Handle {
    window: Window,
    document: Document,
}

impl Handle {
    pub fn default() -> Result<Self> {
        let window: Window = find("window", window)?;
        let document: Document = find("document", || window.document())?;
        let handle = Handle { window, document };
        Ok(handle)
    }

    pub fn get_element_by_id<T>(&self, id: &str) -> Result<T>
    where
        T: Show + js::Cast,
    {
        let elem: T = find(id, || self.document.get_element_by_id(id))?;
        Ok(elem)
    }

    pub(super) fn take(self) -> (Window, Document) {
        let Self {
            window, document, ..
        } = self;
        (window, document)
    }
    pub(super) fn new(window: Window, document: Document) -> Self {
        Self { window, document }
    }
}

/// Helper func for "finding" stuff in the DOM environment.
///
/// Mainly turns Options to Results and provides relevant
/// error messages.
pub fn find<S, F, T, R>(what: S, find: F) -> Result<R>
where
    F: FnOnce() -> Option<T>,
    T: js::Cast + Show + fmt::Debug,
    R: js::Cast + Show,
    S: fmt::Display,
{
    if let Some(t) = find() {
        if let Ok(r) = t.dyn_into() {
            Ok(r)
        } else {
            Err(format!(
                "Something long gone could not be cast from {} to {}",
                T::show(),
                R::show()
            )
            .into())
        }
    } else {
        Err(format!("Not found: {}", what).into())
    }
}
