use super::*;

impl entities::Driver for Handle {
    type Error = Error;
    fn main_loop<F>(self, mut main_loop: F) -> std::result::Result<(), Self::Error>
    where
        F: FnMut(u64) + 'static,
    {
        let Self {
            html, mut last_raf, ..
        } = self;
        let (window, document) = html.take();

        // gonna need 2 windows, one to dispatch RAF here,
        // and one to pass to the next cycle in the callback.
        //
        // itsok because it's only a handle pointer.
        let window2 = window.clone();

        // TODO: Perhaps use a globally allocated closure.
        let clj = Closure::once_into_js(move |mlsec: f64| {
            let mlsec: u64 = mlsec as _;

            let diff = mlsec - last_raf;

            if diff > 200 {
                main_loop(mlsec);
                last_raf = mlsec;
            }

            let html = html::Handle::new(window, document);
            let handle = Handle { html, last_raf };

            if let Ok(_) = handle.main_loop(main_loop) {
            } else {
                // TODO XXX
                panic!()
            }
        });

        // Yes, the "unchecked" part is official recommendation
        // ATM.
        let clj: &js::Function = clj.as_ref().unchecked_ref();

        use js::Cast;
        window2.request_animation_frame(clj)?;

        Ok(())
    }
}
