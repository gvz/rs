mod driver;
#[cfg(feature = "webgl")]
mod gl;
mod html;
mod js;
mod logger;

pub use {gl::Canvas, js::log1, logger::Logger};
#[cfg(feature = "webgl")]
pub type GL = gl::Context;
pub type Error = js::Value;
pub type Result<T> = std::result::Result<T, Error>;
pub mod prelude {
    pub use wasm_bindgen::prelude::*;
}
pub struct Handle {
    html: html::Handle,
    last_raf: u64,
}

use {
    crate::{entities, utils::show::Show, Show},
    prelude::*,
    std::fmt,
};

impl Handle {
    pub fn new() -> Result<Self> {
        let html = html::Handle::default()?;
        let handle = Handle { html, last_raf: 0 };
        Ok(handle)
    }

    pub fn new_engine(&self, canvas_id: &str) -> Result<EngineHandle> {
        let canvas = self.new_canvas(canvas_id)?;
        let logger = self.new_logger()?;
        let ng = Engine::new(canvas, logger);
        let ngh = EngineHandle::new(ng);
        Ok(ngh)
    }

    pub fn new_canvas(&self, canvas_id: &str) -> Result<Canvas> {
        let Self { html, .. } = self;
        let canvas: html::Canvas = html.get_element_by_id(canvas_id)?;
        let ctx: Option<js::Object> = canvas.get_context("webgl")?;
        let ctx: gl::Context = html::find("webgl context", || ctx)?;
        let canvas = Canvas::new(ctx);
        Ok(canvas)
    }

    pub fn new_logger(&self) -> Result<Logger> {
        let logger = Logger {};
        Ok(logger)
    }
}

pub type Engine = entities::Engine<Canvas, Logger>;

#[wasm_bindgen]
#[derive(Clone)]
pub struct EngineHandle {
    engine: Engine,
}

#[wasm_bindgen]
impl EngineHandle {
    fn new(engine: Engine) -> Self {
        Self { engine }
    }

    pub(in crate) fn take(self) -> Engine {
        self.engine
    }

    #[wasm_bindgen]
    pub fn set_canvas_color(&mut self, r: f32, g: f32, b: f32) -> Result<()> {
        self.engine.set_canvas_color(entities::Color::make(r, g, b));
        Ok(())
    }

    #[wasm_bindgen]
    pub fn draw(&self) {
        let Self { engine, .. } = self;
        engine.draw();
    }
}
