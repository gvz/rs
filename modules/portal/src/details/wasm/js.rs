pub use {
    super::{prelude::*, Result},
    js_sys::{Function, Object},
    wasm_bindgen::{JsCast as Cast, JsValue as Value},
    web_sys::console,
};

#[wasm_bindgen]
extern "C" {
    #[wasm_bindgen(js_namespace = console, js_name = log)]
    pub fn log1(s: &str);
}

use super::Show;
Show! { @as_self Object }
