use crate::{
    details::wasm::{self, prelude::*, Result},
    entities::*,
    interactors::studio,
};

/// A handle to the WASM context.
///
/// This context can be used to create engines configured
/// for the WASM environment. Also exposes them as a JS class.
///
/// When this instance is released, all engines and contexts
/// generated through this handle are supposed to be lost.
#[wasm_bindgen]
pub struct Handle {
    wasm: wasm::Handle,
    probe: wasm::EngineHandle,
}
#[wasm_bindgen]
impl Handle {
    /// Initialize the wasm subsystem.
    ///
    /// This is supposed to be the first thing called from JS,
    /// after loading the assembly is done.
    #[wasm_bindgen(constructor)]
    pub fn new(probe_canvas_id: &str) -> Result<Handle> {
        wasm::log1("New wasm handle");

        let wasm = wasm::Handle::new()?;
        let probe = wasm.new_engine(probe_canvas_id)?;
        let handle = Handle { wasm, probe };
        Ok(handle)
    }

    #[wasm_bindgen(getter)]
    pub fn probe(&self) -> wasm::EngineHandle {
        self.probe.clone()
    }

    #[wasm_bindgen]
    pub fn new_engine(&self, canvas_id: &str) -> Result<wasm::EngineHandle> {
        let Self { wasm, .. } = self;
        wasm.new_engine(canvas_id)
    }

    /// [NOT ENGINE]
    ///
    /// Expose the Studio Program Driver.
    #[wasm_bindgen]
    pub fn load_studio_driver(self) -> Result<()> {
        let Self { wasm, probe, .. } = self;
        studio::interact(wasm, probe.take())?;
        Ok(())
    }
}
