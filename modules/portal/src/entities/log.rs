use std::fmt;

pub trait Log {
    fn info<F: fmt::Display>(&self, f: F);
}
