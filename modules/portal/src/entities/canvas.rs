pub trait Canvas {
    fn draw(&self);
    fn set_color(&mut self, color: super::engine::Color);
}
