use super::{canvas, log, math, shapes};

/// The all mightly God.
#[derive(Clone)]
pub struct Engine<Canvas, Log> {
    canvas: Canvas,
    pub log: Log,
}

impl<Canvas, Log> Engine<Canvas, Log>
where
    Canvas: canvas::Canvas,
    Log: log::Log,
{
    /// Yep, it was *that* easy!
    ///
    /// Now it's not.
    pub fn new(canvas: Canvas, log: Log) -> Self {
        Self { canvas, log }
    }

    pub fn draw(&self) {
        let Self { canvas, .. } = self;
        canvas.draw();
    }

    pub fn set_canvas_color(&mut self, color: Color) {
        self.canvas.set_color(color);
    }

    pub fn add_shape() {}
}

pub type Color = math::Vec3<f32>;
