pub trait Driver: Sized {
    type Error;
    fn main_loop<F: 'static>(self, main_loop: F) -> Result<(), Self::Error>
    where
        F: FnMut(u64);
}
