use super::*;

pub struct Turtle {
    pos: Vec3f,
    step: Vec3f,
    trail: Vec<Vec3f>,
}

impl Turtle {
    pub fn new() -> Self {
        Self {
            pos: Vec3f::make(0., 0., 0.),
            step: Vec3f::make(0., 0., 0.),
            trail: Vec::new(),
        }
    }

    pub fn set_step(&mut self, step: Vec3f) {
        self.step = step;
    }

    pub fn set_pos(&mut self, pos: Vec3f) {
        self.pos = pos;
    }

    pub fn step(&mut self) {
        self.trail.push(self.step.clone());
    }
}
