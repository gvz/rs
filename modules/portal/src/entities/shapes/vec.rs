use super::super::math::*;

pub type Mat4f = Mat4<f32>;
pub type Vec3f = Vec3<f32>;

pub type Color = Vec3f;
pub type Position = Vec3f;
pub struct Vertex {
    pub position: Position,
    pub color: Color,
}

pub type Vertices = Box<dyn Iterator<Item = Vertex>>;

pub fn cube() -> Vertices {
    Box::new(
        vec![Vertex {
            position: Position::make(0.5, 0.5, 0.0),
            color: Color::make(0.5, 0.5, 0.0),
        }]
        .into_iter(),
    )
}

pub struct Primitive<V> {
    vertices: V,
    model: Mat4f,
}
