#[derive(Debug, Default, Clone, Copy, Hash, PartialEq, Eq)]
pub struct Vec3<T>([T; 3]);

#[derive(Debug, Default, Clone, Copy, Hash, PartialEq, Eq)]
pub struct Mat4<T>([T; 16]);

impl<T> Vec3<T> {
    pub fn take(self) -> [T; 3] {
        self.0
    }
    pub fn make(x: T, y: T, z: T) -> Self {
        Self([x, y, z])
    }
    pub fn make_diagonal<C: FnMut() -> T>(mut v: C) -> Self {
        Self::make(v(), v(), v())
    }
}

impl<T> Mat4<T> {
    pub fn make_diagonal(one: T, zer: T) -> Self
    where
        T: Copy,
    {
        Self([
            one, zer, zer, zer, zer, one, zer, zer, zer, zer, one, zer, zer, zer, zer, one,
        ])
    }
}
