pub mod awesome {
    pub mod engine {
        use super::{
            map::{self, Map},
            x::*,
        };

        #[derive(Debug, Default, PartialEq, Eq, Hash, Clone)]
        pub struct Id(pub u16);
        pub type Base<T> = HashMap<Id, T>;
        #[derive(Debug, Default)]
        pub struct Score(pub u16);
        #[derive(Debug, Default)]
        pub struct Scoreboard {
            pub me: Score,
            pub opponent: Score,
        }
        #[derive(Debug, Default)]
        pub struct Pacman {
            pub location: map::V2,
        }
        pub type Pacbase = Base<Pacman>;
        pub struct Engine {
            pub map: Map,
            pub pacbase: Pacbase,
            pub me: Id,
            pub score: Scoreboard,
            pub border: Vec<(map::V2, map::Length, map::Length)>,
        }

        impl Engine {
            pub fn new(init_data: Map) -> Self {
                Self {
                    map: init_data,
                    pacbase: Default::default(),
                    me: Default::default(),
                    score: Default::default(),
                    border: Default::default(),
                }
            }
            pub fn not_done(&self) -> bool {
                true
            }
            pub fn set_score<F: FnOnce(&mut Scoreboard) -> R, R>(&mut self, f: F) -> R {
                f(&mut self.score)
            }
            pub fn set_pacbase<F: FnOnce(&mut Id, &mut Pacbase) -> R, R>(&mut self, f: F) -> R {
                self.pacbase.clear();
                f(&mut self.me, &mut self.pacbase)
            }
        }
    }
    pub mod map {
        use super::x::{self};
        #[derive(Debug, Default)]
        pub struct Map {
            dim: V2,
            cells: Vec<Cell>,
        }
        pub type Length = u16;
        #[derive(Debug, Default, Clone)]
        pub struct V2 {
            pub x: Length,
            pub y: Length,
        }
        #[derive(Debug)]
        pub enum Error {
            TooManyCells(Builder),
        }
        pub type Result<T> = x::Result<T, Error>;
        #[derive(Debug)]
        pub enum Poop {
            Opponent(Length),
            Me(Length),
            Clean,
        }
        #[derive(Debug)]
        pub struct Cell {
            pub wall: bool,
            pub food: u16,
            pub poop: Poop,
        }
        #[derive(Debug, Default)]
        pub struct Builder {
            map: Map,
        }
        impl Map {
            pub fn height(&self) -> usize {
                self.dim.y.into()
            }

            fn idx(&self, x: u16, y: u16) -> usize {
                let y: usize = y.into();
                let x: usize = x.into();
                let idx: usize = y * self.height() + x;
                idx
            }

            pub fn get(&self, x: u16, y: u16) -> &Cell {
                &self.cells[self.idx(x, y)]
            }

            pub fn update<R, F>(&mut self, x: u16, y: u16, mut f: F) -> R
            where
                F: FnMut(&mut Cell) -> R,
            {
                let idx = self.idx(x, y);
                f(&mut self.cells[idx])
            }
        }
        impl Builder {
            pub fn new() -> Self {
                Default::default()
            }
            pub fn set_dimentions(&mut self, dim: V2) {
                self.map.dim = dim;
            }
            pub fn flat_len(&self) -> Length {
                self.map.dim.dot()
            }
            pub fn push_cell(&mut self, c: Cell) {
                self.map.cells.push(c);
            }
            pub fn build(self) -> Result<Map> {
                if self.map.cells.len() > self.flat_len().into() {
                    return Err(Error::TooManyCells(self));
                }
                Ok(self.map)
            }
        }
        impl Cell {
            pub fn empty() -> Self {
                Self {
                    wall: false,
                    food: 0,
                    poop: Poop::Clean,
                }
            }
            pub fn wall() -> Self {
                let mut s = Self::empty();
                s.wall = true;
                s
            }
        }
        impl V2 {
            pub fn dot(&self) -> Length {
                self.x * self.y
            }
        }
    }
    pub mod scout {
        Module! { "scout" }
        use super::{engine, map, x::*};

        #[derive(Debug, Clone, Copy)]
        pub enum Conclusion {
            None,
            Suicide,
            Border {
                my_distance: map::Length,
                opponent_distance: map::Length,
            },
        }
        #[derive(Debug, Default)]
        pub struct Scout {
            pos: map::V2,
            conclusion: Conclusion,
            traveled_distance: map::Length,
            friendly: bool,
        }

        #[derive(Debug, Default)]
        pub struct Scouts {
            scouts: Deque<Scout>,
        }

        impl Scout {
            pub fn scout<'a>(&'a self, map: &'a map::Map) -> impl FnMut() -> Option<Scout> + 'a {
                let &Self {
                    friendly,
                    pos: map::V2 { x, y },
                    traveled_distance,
                    conclusion,
                    ..
                } = self;

                let mut n = 0_i32;
                move || loop {
                    Func! { "Scout::scout::()"; "{} {:?}", n, conclusion }
                    let is_dead = match conclusion {
                        Conclusion::None => false,
                        Conclusion::Suicide => true,
                        Conclusion::Border { .. } => true,
                    };
                    if is_dead {
                        t!("is dead");
                        break None;
                    }

                    if n == 4 {
                        t!("generator done");
                        break None;
                    }

                    let mut j = n / 2;
                    let mut i = n % 2;
                    n += 1;

                    if j == 0 {
                        j = -1;
                    }
                    if i == 0 {
                        i = -1;
                    }

                    if x == 0 && i == -1 {
                        i = 0;
                    }
                    if y == 0 && j == -1 {
                        j = 0;
                    }
                    if i == 0 && j == 0 {
                        continue;
                    }

                    let x: u16 = (Into::<i32>::into(x) + i) as u16;
                    let y: u16 = (Into::<i32>::into(y) + j) as u16;
                    let cell = map.get(x, y);

                    if cell.wall {
                        t!("hit wall ({},{})", x, y);
                        continue;
                    }

                    t!("next warrior");
                    break Some(Scout {
                        pos: map::V2 { x, y },
                        conclusion: Conclusion::None,
                        traveled_distance: traveled_distance + 1,
                        friendly,
                    });
                }
            }
            pub fn on_scout(&mut self, map: &mut map::Map) {
                Func! { "Scout::on_scout"; "{:?}", self }
                // Copied values
                let &mut Self {
                    pos: map::V2 { x, y },
                    traveled_distance,
                    friendly,
                    ..
                } = self;
                // Mut refs
                let Self { conclusion, .. } = self;
                map.update(x, y, |cell| {
                    let my_poop = || {
                        if friendly {
                            map::Poop::Me(traveled_distance)
                        } else {
                            map::Poop::Opponent(traveled_distance)
                        }
                    };
                    match (&*cell).poop {
                        map::Poop::Clean => {
                            let poop = my_poop();
                            t!("pooping with {:?}", poop);
                            cell.poop = poop;
                        }
                        map::Poop::Me(distance_a) => {
                            if distance_a >= traveled_distance {
                                *conclusion = Conclusion::Suicide;
                            } else {
                                cell.poop = my_poop();
                            }
                        }
                        map::Poop::Opponent(distance) => {
                            *conclusion = Conclusion::Border {
                                my_distance: traveled_distance,
                                opponent_distance: distance,
                            };
                        }
                    }
                });
            }
        }

        pub fn scout_rec<F>(map: &mut map::Map, scouts: Scouts, mut callb: F) -> Scouts
        where
            F: FnMut(Scout),
        {
            Func! { "scout_rec"; "{}", scouts.scouts.len() }
            let Scouts { mut scouts } = scouts;
            let c = scouts.len();
            for _ in 0..c {
                let mut scout = scouts.pop_front().unwrap();
                scout.on_scout(map);
                {
                    let mut gen = scout.scout(map);
                    while let Some(scout) = gen() {
                        t!("new scout: {:?}", scout);
                        scouts.push_back(scout);
                    }
                }
                (&mut callb)(scout);
            }
            Scouts { scouts }
        }

        pub fn scout(mut ng: engine::Engine) -> engine::Engine {
            Func! { "scout"; "" }
            let mut scouts = Scouts {
                scouts: ng
                    .pacbase
                    .iter()
                    .map(|(id, engine::Pacman { location, .. })| Scout {
                        friendly: id.clone() == ng.me,
                        conclusion: Conclusion::None,
                        traveled_distance: 0,
                        pos: location.clone(),
                    })
                    .map(|s| {
                        t!("scout: {:?}", s);
                        s
                    })
                    .collect(),
            };
            assert!(scouts.scouts.iter().find(|s| s.friendly).is_some());
            assert!(scouts.scouts.iter().find(|s| !s.friendly).is_some());

            let mut border = ng.border;
            let mut map = ng.map;

            let mut callb = |mut scout: Scout| {
                Func! { "scout::callb"; "" }
                match scout.conclusion {
                    Conclusion::Border {
                        my_distance,
                        opponent_distance,
                    } => {
                        let (me, other) = if scout.friendly {
                            (my_distance, opponent_distance)
                        } else {
                            (opponent_distance, my_distance)
                        };
                        let bord = (scout.pos, me, other);
                        t!("push border: {:?}", bord);
                        border.push(bord);
                    }
                    _ => (),
                }
            };

            while !scouts.scouts.is_empty() {
                scouts = scout_rec(&mut map, scouts, &mut callb);
            }
            ng.border = border;
            ng.map = map;
            ng
        }

        impl Default for Conclusion {
            fn default() -> Self {
                Self::None
            }
        }
    }
    pub mod stage {
        use super::{engine, scout};

        pub type Mod = fn(engine::Engine) -> engine::Engine;
        pub type Op = Mod;
        pub type Stage = &'static [Op];

        pub const SCOUT: Op = scout::scout;
        pub const PREPROC: Stage = &[SCOUT];

        pub trait StageLike {
            fn apply(self, ng: engine::Engine) -> engine::Engine;
        }
        impl<S, T> StageLike for S
        where
            Self: IntoIterator<Item = T>,
            T: Fn(engine::Engine) -> engine::Engine,
        {
            fn apply(self, mut ng: engine::Engine) -> engine::Engine {
                for op in self {
                    ng = op(ng);
                }
                ng
            }
        }
    }

    pub mod driver {
        Module! { "driver" }
        use super::{
            data_loader,
            engine::Engine,
            map, serde,
            stage::{self, StageLike},
            x::*,
        };
        fn _drive() -> data_loader::Result<()> {
            Func! { "drive"; "" }
            let mut sd = serde::new(io::stdin());
            let mut ng = Engine::new(data_loader::load_init_data(&mut sd)?);

            while ng.not_done() {
                data_loader::load_step_data(&mut sd, &mut ng)?;

                ng = stage::PREPROC.apply(ng);

                if ng.border.is_empty() {
                    panic!("Empty border");
                }
                let mut mv = (map::V2 { x: 0, y: 0 }, 1u16 << 15, 0 as map::Length);
                for (pos, d0, d1) in ng.border.iter() {
                    let pos = pos.clone();
                    let d0 = d0.clone();
                    let d1 = d1.clone();

                    if d1 > mv.2 {
                        mv = (pos, d0, d1);
                    }
                }

                println!("MOVE {id} {x} {y}", id = ng.me.0, x = mv.0.x, y = mv.0.y);
            }

            Ok(())
        }

        pub fn drive() {
            _drive().unwrap()
        }
    }

    pub mod data_loader {
        Module!("data_loader");

        use super::{
            engine,
            map::{self, Map},
            serde::{self, Serde},
            x::{self, *},
        };

        #[derive(Debug)]
        pub enum Error {
            Sd(Str, serde::Error),
            Map(Str, map::Error),
            UnknownMapChar(char),
            PrematureEndOf(Str),
        }
        pub type Result<T> = x::Result<T, Error>;

        pub fn load_init_data(sd: &mut Serde) -> Result<Map> {
            Func!("load_init_data"; "");
            let err_sd = |err| Error::Sd(__FUNC_NAME, err);
            let err_map = |err| Error::Map(__FUNC_NAME, err);

            let mut load_map = || -> Result<Map> {
                let mut map = map::Builder::new();
                {
                    let x = sd.read("map.width").map_err(err_sd)?;
                    let y = sd.read("map.height").map_err(err_sd)?;
                    let dim = map::V2 { x, y };
                    map.set_dimentions(dim);
                }

                let mut i = map.flat_len();
                loop {
                    if i <= 0 {
                        break;
                    }
                    let line = sd.read_line("map.cell").map_err(err_sd)?;

                    t!("new line {}/{}", i, map.flat_len());
                    for c in line.chars() {
                        progress!(map.flat_len() - i, map.flat_len());
                        match c {
                            ' ' => map.push_cell(map::Cell::empty()),
                            '#' => map.push_cell(map::Cell::wall()),
                            c => return Err(Error::UnknownMapChar(c)),
                        }
                        i -= 1;
                        if i <= 0 {
                            break;
                        }
                    }
                    trace_log!("");

                    sd.consume();
                }
                let map = map.build().map_err(err_map)?;
                Ok(map)
            };

            let map = load_map()?;
            Ok(map)
        }
        pub fn load_step_data(sd: &mut Serde, ng: &mut engine::Engine) -> Result<()> {
            Func!("load_step_data"; "");
            let err_sd = |err| Error::Sd(__FUNC_NAME, err);

            fn load_score(sd: &mut Serde, score: &mut engine::Scoreboard) -> serde::Result<()> {
                score.me = engine::Score(sd.read("score.me")?);
                score.opponent = engine::Score(sd.read("score.opponent")?);
                Ok(())
            }

            fn load_pacman(sd: &mut Serde) -> serde::Result<engine::Pacman> {
                let pacman = engine::Pacman {
                    location: map::V2 {
                        x: sd.read("position in the grid (x)")?,
                        y: sd.read("position in the grid (y)")?,
                    },
                };
                Ok(pacman)
            }

            fn load_pacbase(
                sd: &mut Serde,
                my_id: &mut engine::Id,
                pacbase: &mut engine::Pacbase,
            ) -> serde::Result<()> {
                let count = sd.read::<u16>("visible pac count")?;
                for _ in 0..count {
                    let id = engine::Id(sd.read("[id] pac number (unique withing a team)")?);

                    if sd.read::<u16>("[me] true if this pac is yours")? != 0 {
                        *my_id = id.clone();
                    }

                    pacbase.insert(id, load_pacman(sd)?);

                    // eat up rest
                    let _type_id = sd.read_token("[type id] unused in wood leagues")?;
                    let __speed_turns_left =
                        sd.read_token("[speed turns left] unused in wood leagues")?;
                    let _ability_cooldown =
                        sd.read_token("[ability cooldown] unused in wood leagues")?;
                }

                Ok(())
            }

            fn load_food(sd: &mut Serde, map: &mut map::Map) -> serde::Result<()> {
                let count = sd.read::<u16>("visible pellet count")?;
                for _ in 0..count {
                    let points = sd.read("food points")?;
                    let x = sd.read("food x")?;
                    let y = sd.read("food y")?;
                    map.update(x, y, |cell| cell.food = points);
                }
                Ok(())
            }

            let mut io_errors = || -> serde::Result<()> {
                ng.set_score(|score| load_score(sd, score))?;
                ng.set_pacbase(|id, pacbase| load_pacbase(sd, id, pacbase))?;
                load_food(sd, &mut ng.map)?;

                Ok(())
            };

            io_errors().map_err(err_sd)?;
            Ok(())
        }
    }
    pub mod serde {
        Module!("serde");

        use super::x::{self, *};

        pub fn new<R: io::Read + 'static>(r: R) -> Serde {
            Serde {
                buf: dft(),
                token: 0..0,
                rest: 0,
                inp: Box::new(io::BufReader::new(r)),
                history: Default::default(),
            }
        }
        pub struct Serde {
            buf: String,
            token: Range<usize>,
            rest: usize,
            inp: Box<dyn io::BufRead>,
            history: Vec<String>,
        }
        #[derive(Debug)]
        pub enum Error {
            Parse(String, String),
            Io(String, io::Error),
        }
        pub type Result<T> = x::Result<T, Error>;
        impl Serde {
            pub fn read_line(&mut self, what: &str) -> Result<&str> {
                Func! { "read_line"; "{}", what }
                if self.is_buffer_empty() {
                    t!("buffer is empty");
                    self.fill_buffer(what)?;
                }

                let line = &self.buf;
                self.history.push(self.buf.clone());
                Ok(line)
            }
            pub fn read_token(&mut self, what: &str) -> Result<&str> {
                Func! { "read_token"; "{}", what }
                self.read_line(what)?;

                t!("advancing on target");
                self.advance_token();

                let t = self.borrow_token();
                Ok(t)
            }
            pub fn read<T>(&mut self, what: &str) -> Result<T>
            where
                T: FromStr,
                T::Err: fmt::Debug,
            {
                Func! { "read"; "{}",  what }
                let t = self.read_token(what)?;

                t!("parsing {}: {:?}", what, t);
                let t = self.parse_token(what)?;

                Ok(t)
            }
            fn borrow_token(&self) -> &str {
                &self.buf[self.token.clone()]
            }
            fn parse_token<T>(&self, what: &str) -> Result<T>
            where
                T: FromStr,
                T::Err: fmt::Debug,
            {
                let s = self.borrow_token();
                let t = T::from_str(s)
                    .map_err(|err| Error::Parse(format!("{}: {:?}", what, err), s.to_owned()));
                Ok(t?)
            }
            fn is_buffer_empty(&self) -> bool {
                self.rest == self.buf.len()
            }
            pub fn consume(&mut self) {
                self.rest = self.buf.len();
            }
            fn advance_token(&mut self) {
                let start = self.rest;
                let end = if let Some(n) = self.buf[start..].find(' ') {
                    start + n
                } else {
                    self.buf.len()
                };
                self.rest = [(end + 1), self.buf.len()].iter().min().cloned().unwrap();
                self.token = start..end;
            }
            fn fill_buffer(&mut self, what: &str) -> Result<()> {
                self.buf.clear();
                self.inp
                    .read_line(&mut self.buf)
                    .map_err(|err| Error::Io(format!("{}: read input line", what), err))?;
                self.buf.truncate(self.buf.trim_end_matches('\n').len());
                self.token = 0..0;
                self.rest = 0;
                Ok(())
            }
        }
    }
    pub mod x {
        pub type Str = &'static str;
        pub use {
            crate::{progress, t, trace_log, Func, Module},
            std::{
                collections::{HashMap, VecDeque as Deque},
                fmt, io,
                ops::Range,
                result::Result,
                str::FromStr,
            },
        };

        pub fn dft<T: Default>() -> T {
            <_>::default()
        }

        pub mod cols {
            pub const VVDIM: u8 = 237;
            pub const VDIM: u8 = 243;
            pub const DIM: u8 = 249;
            pub const WOOMB: u8 = 217;
            pub const BRIGHT_PINK: u8 = 224;

            pub mod themes {
                pub mod a {
                    pub const MOD: u8 = 78;
                    pub const FUNC: u8 = 81;
                }
            }
        }
        pub fn col<S, R>(c: u8, s: S) -> impl FnOnce(&mut dyn FnMut(&dyn fmt::Display) -> R) -> R
        where
            S: fmt::Display,
        {
            move |f| f(&format_args!("\x1b[38:5:{col}m{s}\x1b[m", col = c, s = s))
        }
        #[macro_export]
        macro_rules! trace_log {
            ($($t:tt)*) => {
                eprint!($($t)*)
            };
        }
        #[macro_export]
        macro_rules! progress {
            ($c:expr, $m:expr) => {
                col(cols::BRIGHT_PINK, "[")(&mut |op| {
                    col(cols::BRIGHT_PINK, "]")(&mut |cls| {
                        col(cols::WOOMB, ".")(&mut |ind| {
                            let c: f64 = $c.into();
                            let m: f64 = $m.into();
                            let scale = 100.0 / 2.0;
                            let n = c / m;
                            let p = n * scale;
                            let p = p as u16;
                            trace_log!("\r{}{:03}/{:03}", op, p, scale);
                            for _ in 0..p {
                                trace_log!("{}", ind)
                            }
                            trace_log!("{}", cls);
                        })
                    })
                })
            };
        }
        #[macro_export]
        macro_rules! t {
            ($($t:tt)*) => {
                col(cols::VDIM, __MOD_NAME)(&mut |mod_| {
                    col(cols::DIM, __FUNC_NAME)(&mut |func| {
                        col(cols::VVDIM, " ..")(&mut |ind| {
                            trace_log!("{}{}::{} {}\n", ind, mod_, func, format_args!($($t)*))
                        });
                    })
                })
            };
        }
        #[macro_export]
        macro_rules! Module {
            ($name:expr) => {
                const __MOD_NAME: &str = $name;
            };
        }
        #[macro_export]
        macro_rules! Func {
            ($name:expr; $($args:tt)*) => {
                const __FUNC_NAME: &str = $name;
                struct __FuncExitHook;
                impl std::ops::Drop for __FuncExitHook {
                    fn drop(&mut self) {
                        __func_path_color(|f| trace_log!("-- {}\n", f))
                    }
                }
                let __func_exit_hook = __FuncExitHook;
                fn __func_path_color<C: FnMut(&dyn fmt::Display) -> R, R>(mut c: C) -> R {
                    col(cols::themes::a::MOD, __MOD_NAME)(&mut |mod_| {
                        col(cols::themes::a::FUNC, __FUNC_NAME)(&mut |func| {
                            c(&format_args!("{}::{}", mod_, func))
                        })
                    })
                }
                __func_path_color(|f| trace_log!("{} {}\n", f, format_args!($($args)*)));
            };
        }
    }
}
pub fn main() {
    awesome::driver::drive();
}
