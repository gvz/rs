use crate::entities;

pub fn interact<D, C, L>(driver: D, mut ng: entities::Engine<C, L>) -> Result<(), D::Error>
where
    D: entities::Driver,
    C: entities::Canvas + 'static,
    L: entities::Log + 'static,
{
    use std::f32::consts::*;
    ng.log.info("I'm drivin nigga");

    driver.main_loop(move |milliseconds| {
        let s = milliseconds as f32;
        let s = (s / 500.) * FRAC_PI_4;
        let s = s.sin();

        let n = |f| (1.0 + f) * 0.5;
        let n = |f| (n(f) * 0.1 + 0.1);
        let r = n(s);
        let g = n(s);
        let b = n(s);
        let color = entities::Color::make(r, g, b);

        ng.set_canvas_color(color);
        ng.draw();
    })?;

    Ok(())
}
