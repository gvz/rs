use std::fmt;

pub trait Show {
    type Show: ?Sized + fmt::Display + 'static;
    fn show() -> &'static Self::Show;
}

#[macro_export]
macro_rules! Show {
    (@as_self $name:ident) => {
        impl Show for $name {
            type Show = str;
            fn show() -> &'static str {
                const NAME: &str = stringify!($name);
                NAME
            }
        }
    };
}
