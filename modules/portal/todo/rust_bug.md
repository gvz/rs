```rust
fn main() {}

const fn f<T: ?Sized>() -> usize {
    4
}

pub struct M<N, T> {
    pub d: [T; f::<N>()],
}
```
