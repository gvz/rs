use {
    std::{
        borrow::BorrowMut,
        fmt, fs, io,
        os::unix::{
            fs::PermissionsExt,
            net::{UnixListener, UnixStream},
        },
        path::Path,
        thread,
    },
    x::Error,
};

fn unix_listener() -> Result<UnixListener> {
    let path = "/u/_.the_common_web/usox/01";

    if Path::new(path).exists() {
        eprintln!(" * Socket exists, removing: {}", path);
        fs::remove_file(path).unwrap();
    }

    let listener = UnixListener::bind(path).unwrap();
    eprintln!(" * Created socket: {}", path);

    let mut perm = fs::metadata(path).unwrap().permissions();
    eprintln!(" * Set socket mode 777");
    perm.set_mode(0o777);
    fs::set_permissions(path, perm).unwrap();

    Ok(listener)
}

fn main() {
    let conns = unix_listener().unwrap();
    let mut threads = Vec::new();
    loop {
        eprintln!(". Waiting for connections...");
        let stream = conns.accept();
        eprintln!(". . Connection accepted");
        match stream {
            Ok((stream, _)) => {
                eprintln!(". . . Serving it");
                threads.push(thread::spawn(|| handle_client(stream)));
            }
            Err(err) => {
                eprintln!(". . . It was pooped: {:?}", err);
                break;
            }
        }
    }

    for thread in threads {
        thread.join().unwrap().unwrap();
    }
    eprintln!(". Program Terminated");
}

fn handle_client(stream: UnixStream) -> Result<()> {
    eprintln!(". . . . In case you are wondering, I am also here.");
    use fmt::Write as _;
    use io::Write as _;

    let mut reply = String::with_capacity(4096);
    write!(
        reply,
        r#"<!HTML> <html>
            <head>
                <Title> Title? Fuck you! </title>
            </head>
            <body>
                <p> This is the Zero One's </p>
                <input type="file">Input file</input>
                <table><tbody>
            "#
    )?;

    let mut netstring = NetStringCodec::new(io::BufReader::new(stream));
    if let Some(string) = netstring.next()? {
        let mut headers = HeadersCodec::new(string);
        while let Some((name, value)) = headers.next()? {
            eprintln!(" -- {}: {}", name, value);
            write!(reply, "<tr> <th>{}</th> <td>{}</td> </tr>", name, value)?;

            if name == "DOCUMENT_URI" && value == "/01/reset" {
                std::process::exit(0);
            }
        }

        writeln!(
            reply,
            "
                </table>
            </body>
        </html>"
        )?;

        let headers = format!(
            "Context-length: {cont_len}{nl
        }Content-type: text/html{nl
        }{nl
        }{body}",
            cont_len = reply.len(),
            nl = "\r\n",
            body = reply,
        );

        let mut stream = netstring.into_inner().into_inner();
        write!(stream, "{}", headers)?;
    } else {
        eprintln!(". . . . . No next netstring -- did nothing");
    }

    eprintln!(". . . . Mission Complete");
    Ok(())
}

mod errors {
    use super::*;
    pub type Io = io::Error;
    pub type Utf = std::str::Utf8Error;
    pub type NumParse = std::num::ParseIntError;
    pub type Fmt = fmt::Error;
    #[derive(Debug)]
    pub struct HeaderValueMissing;
}
Error! {
    Io, Utf, NumParse, HeaderValueMissing, Fmt
    @derive Debug
}
type Result<T> = std::result::Result<T, Error>;

struct NetStringCodec<R: io::BufRead> {
    input: R,
    buffer: Vec<u8>,
    string: String,
}
impl<R: io::BufRead> NetStringCodec<R> {
    pub fn new(input: R) -> Self {
        Self {
            input,
            buffer: Default::default(),
            string: Default::default(),
        }
    }
    pub fn into_inner(self) -> R {
        self.input
    }
    pub fn next(&mut self) -> Result<Option<&str>> {
        self.input.read_until(b':', &mut self.buffer)?;
        let len = std::str::from_utf8(&self.buffer)?;
        let strlen = len.trim_end_matches(':');
        let strlen = strlen.parse()?;
        self.buffer.resize(strlen, 0u8);
        self.input.read_exact(&mut self.buffer)?;
        let string = std::str::from_utf8(&self.buffer)?;
        self.string.replace_range(.., string);
        Ok(Some(&self.string))
    }
}
struct HeadersCodec<'a> {
    input: &'a str,
    cursor: usize,
}
impl<'a> HeadersCodec<'a> {
    pub fn new(input: &'a str) -> Self {
        Self {
            input,
            cursor: Default::default(),
        }
    }
    pub fn next(
        &mut self,
    ) -> std::result::Result<Option<(&'a str, &'a str)>, errors::HeaderValueMissing> {
        let Self { input, cursor } = self;

        if *cursor >= input.len() {
            return Ok(None);
        }

        let mut tokens = input[*cursor..].split('\x00');

        if let Some(name) = tokens.next() {
            *cursor += name.len() + 1;

            if let Some(value) = tokens.next() {
                *cursor += value.len() + 1;

                return Ok(Some((name, value)));
            }

            return Err(errors::HeaderValueMissing);
        }

        Ok(None)
    }
}

pub trait Inspect {
    fn inspect(&self) -> String;
}
impl<T: fmt::Debug> Inspect for T {
    fn inspect(&self) -> String {
        format!("{:?}", self)
    }
}
