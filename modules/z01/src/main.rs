use {
    errors::{self as err, Result},
    std::result,
};

fn main() -> Result<()> {
    println!("Veg");

    let config = config::load()?;

    Ok(())
}

pub mod env {
    #[derive(Debug)]
    pub struct Error<'a>(pub &'a str);

    pub struct Get<'a>(pub &'a str);
    impl<'a> Get<'a> {
        pub fn get(&self) -> Result<String, Error<'_>> {
            let Self(name) = self;
            std::env::var(name).map_err(|_| Error(name))
        }
    }

    pub const SCGI_SOCKADDR: Get = Get("Z1_SCGI_SOCKADDR");
}

pub mod errors {
    use super::{env, errors, result};
    pub type Result<T> = result::Result<T, Error>;

    pub type VarError = env::Error<'static>;

    use ::x::Error; // macro
    Error! {
        VarError
        @derive Debug
    }
}

pub mod config {
    use super::{env, Result};
    pub struct Config {
        sockaddr: String,
    }

    pub fn load() -> Result<Config> {
        let config = Config {
            sockaddr: env::SCGI_SOCKADDR.get()?,
        };
        Ok(config)
    }
}
