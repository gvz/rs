pub mod show;
pub mod stream;

/// Easy error type aggreggator.
///
/// # Examples:
/// ```rust
/// use crate::x::Error;
///
/// pub mod errors {
///     pub type Lol = String;
///     pub type Io = std::io::Error;
///     pub type Fmt = std::fmt::Error;
///     pub type Inf = std::convert::Infallible;
/// }
/// Error!(Lol, Io, Fmt, Inf);
/// ```
#[macro_export]
macro_rules! Error {
    ($($name:ident),* $(@derive $($der:ident),+)?) => {
        $( #[derive( $($der),+ )] )?
        pub enum Error {
            $( $name(errors::$name) ),*
        }

        $(
        impl From<errors::$name> for Error {
            fn from(err: errors::$name) -> Self {
                Self::$name(err)
            }
        }
        )*
    };
}

/// Easy string-pool.
///
/// # Exaples
/// ```rust
/// use x::pool;
/// let mut pool = pool::Pool::<String>::default();
///
/// let s = "Hello.";
/// let insert0 = pool.whirl(|needle| needle.replace_range(.., s));
/// let insert1 = pool.whirl(|needle| needle.replace_range(.., s));
///
/// assert_eq!(insert0, insert1);
///
/// ```
pub mod pool {
    #[derive(Default)]
    pub struct Pool<T>
    where
        T: Eq + std::hash::Hash + Clone,
    {
        set: std::collections::HashSet<std::rc::Rc<T>>,
        needle: std::rc::Rc<T>,
    }
    impl<T> Pool<T>
    where
        T: Eq + std::hash::Hash + Clone,
    {
        pub fn whirl<S>(&mut self, set_search_value: S) -> std::rc::Rc<T>
        where
            S: FnOnce(&mut T),
        {
            use std::rc::Rc;

            let needle = &mut self.needle;
            {
                let needle = Rc::get_mut(needle).expect("BUG in using needle -- second borrow");
                set_search_value(needle);
            }

            if let Some(rc) = self.set.get(needle) {
                rc.clone()
            } else {
                let obj = T::clone(&*needle);
                let rc = Rc::new(obj);
                self.set.insert(rc.clone());
                rc
            }
        }
    }
    pub fn string_setter<S: AsRef<str>>(src: S) -> impl Fn(&mut String) {
        move |trg| trg.replace_range(.., src.as_ref())
    }
}

/// Make a type Debug-able although it isn't.
///
/// Rely on type being at least [`Show`]-able in order to do that.
///
/// Be as transparent as possible otherwise.
pub mod fakebug {
    pub struct Bug<'a, Life: ?Sized>(pub &'a mut Life);

    macro_rules! fakeplement {
        ($(. $tps:ident)* $name:path) => {
            impl <'a, Life $(, $tps)* > $name for Bug<'a, Life>
            {
                fn read(&mut self, _: &mut [u8]) -> std::result::Result<usize, std::io::Error> { todo!() }
            }
        };
    }

    fakeplement!(std::io::Read);
    crate::show!(Bug<'a Life>);
    crate::show![for debug: Bug<'a Life>];
}
