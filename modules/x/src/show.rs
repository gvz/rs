//! Show the actual type to a stream.
//!
//! With rust `Debug`, types are erased in the output string.
//! `Show` is made to preserve those.
//!
//! The easiest way to implement show is with the `show!` macro,
//! which simply receives the type signature as an argument.
//!
//! `Show` provides a `show` method, that writes the type's
//! string's bytes to a given `fmt::Write`.
use std::fmt::{self, Result, Write};

/// Collect the `Show` of `T` into a `String`.
pub fn string_of<T: ?Sized + Show>() -> String {
    let mut s = String::new();
    <T as Show>::show(&mut s).expect("io error writing to membuf");
    s
}
/// Collect the `Show` of type `T` of given value into a `String`.
pub fn string_of_val<T: ?Sized + Show>(_: &T) -> String {
    string_of::<T>()
}

/// Show the type of `Self` by writting it into `trg`.
pub trait Show {
    fn show(trg: &mut dyn Write) -> Result;
}

pub struct Shower<'a, T>(pub &'a T);
//impl<'a, T> Shower<'a, T>
//where
//    T: Show,
//{
//    fn write_to(&self, f: &mut dyn Write) -> Result {
//        T::show(f)
//    }
//}
impl<'a, T> fmt::Display for Shower<'a, T>
where
    T: Show,
{
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", string_of_val(self.0))
    }
}

/// Implement `Show` recursively for a type with type parameters.
///
/// # Examples
/// ```rust
/// # #[macro_use] extern crate x;
/// # use {std::{io, marker::PhantomData as __}, x::show::{self, string_of}};
///
/// // Works with Sized types only :-/
/// struct A;
/// struct B;
/// struct C<A: ?Sized, B: ?Sized>(__<(*const A, B,)>);
///
/// show! { A }
/// show! { B }
/// show! { C<A, B> }
/// assert_eq!(string_of::<C<A, B>>(), "C<A, B>");
/// ```
#[macro_export]
macro_rules! show {
    ( $name:ident $(< $($lfs:lifetime),* $($tps:ident),* >)? ) => {
        impl $(<
               $($lfs,)*
               $($tps: ?Sized + $crate::show::Show,)*
        >)? $crate::show::Show for $name $(<
                $($lfs,)*
                $($tps,)*
        >)?
        {
            fn show(trg: &mut dyn std::fmt::Write) -> std::fmt::Result {
                const SELF: &str = stringify!($name);
                write!(trg, "{}", SELF)?;

                $(
                    let mut sep_ = "";
                    let mut sep = |trg: &mut dyn std::fmt::Write| -> std::fmt::Result {
                        write!(trg, "{}", sep_)?;
                        sep_ = ", ";
                        Ok(())
                    };

                    write!(trg, "<")?;
                    $(
                        sep(trg)?;
                        <$tps as $crate::show::Show>::show(trg)?;
                    )+
                    write!(trg, ">")?;
                )?

                Ok(())
            }
        }
    };
    (for debug: $name:ident $(< $($lfs:lifetime),* $($tps:ident),* >)? ) => {
        impl $(< $($lfs,)* $($tps),* >)? std::fmt::Debug for $name $(< $($lfs,)*  $($tps),* >)?
        where
            Self: $crate::show::Show,
        {
            fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
                <Self as $crate::show::Show>::show(f)
            }
        }
    };
    (dyn $name:path) => {
        impl $crate::show::Show for dyn $name
        {
            fn show(trg: &mut dyn std::fmt::Write) -> std::fmt::Result {
                const SELF: &str = stringify!($name);
                write!(trg, "{}", SELF)?;
                Ok(())
            }
        }
    };
}

impl Show for () {
    fn show(trg: &mut dyn Write) -> Result {
        write!(trg, "()")
    }
}
impl<A> Show for (A,)
where
    A: Show,
{
    fn show(trg: &mut dyn Write) -> Result {
        write!(trg, "(")?;
        A::show(trg)?;
        write!(trg, ")")?;
        Ok(())
    }
}
impl<A, B> Show for (A, B)
where
    A: Show,
    B: Show,
{
    fn show(trg: &mut dyn Write) -> Result {
        write!(trg, "(")?;
        A::show(trg)?;
        write!(trg, ", ")?;
        B::show(trg)?;
        write!(trg, ")")?;
        Ok(())
    }
}
impl<'a, T: Show> Show for &'a T {
    fn show(trg: &mut dyn Write) -> Result {
        write!(trg, "&'a ")?;
        T::show(trg)?;
        Ok(())
    }
}
impl<'a, T: Show> Show for &'a mut T {
    fn show(trg: &mut dyn Write) -> Result {
        write!(trg, "&'a mut ")?;
        T::show(trg)?;
        Ok(())
    }
}
show!(Box<T>);
show!(dyn std::io::Read);
