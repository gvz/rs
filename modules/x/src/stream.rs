#[test]
fn example() -> Result<(), ()> {
    Ok(())
}

use std::result;

pub use self::prov0::*;
mod prov0 {
    use super::result;

    pub type Stream<T, E> = result::Result<Option<T>, E>;
    pub type Nxt<N> = Stream<next::Next<N>, next::Error<N>>;

    pub trait Next {
        type Error;
        type Next;
        fn next(self) -> Nxt<Self>;
    }
    pub mod next {
        use super::Next as Nx;
        pub type Error<N> = <N as Nx>::Error;
        pub type Next<N> = <N as Nx>::Next;
    }

    impl<F, T, E> Next for F
    where
        F: FnOnce() -> Stream<T, E>,
    {
        type Error = E;
        type Next = T;
        fn next(self) -> Nxt<Self> {
            self()
        }
    }
}
