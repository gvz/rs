use std::{
    borrow::{Borrow, BorrowMut},
    fs,
    io::{self, Read},
    process, thread,
};
fn spawn<C: BorrowMut<Cmd>>(name: &str, mut cmd: C) -> process::Child {
    eprintln!(" +[{}] {:?}", name, cmd.borrow());
    cmd.borrow_mut()
        .spawn()
        .expect(&format!("spawn {}: {:?}", name, cmd.borrow()))
}
fn out(name: &str, child: process::Child) -> process::ChildStdout {
    child.stdout.expect(&format!("{} stdout", name))
}
fn err(name: &str, child: process::Child) -> process::ChildStderr {
    child.stderr.expect(&format!("{} stderr", name))
}
fn main() {
    let mut stdout = std::io::stdout();
    let stdout = &mut stdout;

    let check = spawn("check", cargo_check());
    let extract = spawn("extract", extract(err("check", check)));
    //let mut plan = spawn("plan", plan(out("extract", extract)));
    //let plan_out = plan.stdout.as_mut().expect("plan stdout");
    let out = &mut out("c", extract);
    std::io::copy(out, stdout).expect("flush out");

    //let status = plan.wait().expect("wait for plan");
    //eprintln!("Plan exited: {:?}", status);
}

use std::process::{Command as Cmd, Stdio};

fn cmd(name: &str) -> Cmd {
    let mut cmd = Cmd::new(name);
    cmd.stdin(Stdio::piped());
    cmd.stdout(Stdio::piped());
    cmd.stderr(Stdio::inherit());
    cmd
}

fn cargo_check() -> Cmd {
    let mut cmd = cmd("cargo");
    cmd.stdin(Stdio::null());
    cmd.stdout(Stdio::null());
    cmd.stderr(Stdio::piped());
    cmd.args(&["check", "--all-targets", "--profile", "test"]);
    cmd
}

fn extract<I: Into<Stdio>>(stdin: I) -> Cmd {
    let mut cmd = cmd("jq");
    cmd.args(&[
        "--raw-input",
        "--slurp",
        "--arg",
        "rgx",
        extract_rgx(),
        extract_jq(),
    ]);
    cmd.stdin(stdin);
    cmd
}

fn plan<I: Into<Stdio>>(stdin: I) -> Cmd {
    let mut cmd = cmd("jq");
    cmd.args(&["--raw-output", patch_jq()]);
    cmd.stdin(stdin);
    cmd
}

const fn patch_jq() -> &'static str {
    include_str!("../rc/impltr.plan.jq")
}
const fn extract_jq() -> &'static str {
    include_str!("../rc/impltr.extract.jq")
}
const fn extract_rgx() -> &'static str {
    include_str!("../rc/impltr.rgx")
}
