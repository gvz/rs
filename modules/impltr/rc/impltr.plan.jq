#!/usr/bin/env -S jq --raw-output --rawfile rgx impltr.rgx -f
# vim: et ts=2 sw=2
# https://regex101.com/r/0OQjpj/2
# error\[E0046]:.*\R  --> (?'file'[^:]+):(?'line'[^:]+):(?'column'.*)\R([\s\d]+\|.*\R)*[\s*=]*help:[^:]*:\s*`(?'fix'[^`]+)`

def option_1:.
| @sh "printf '%s' \($impl) | _.cargoi/bin/impltr -- \($file) \($line) \($column)"
|.;

.
| . as { $file, $line, $column, $impl }
| ($line | tonumber) as $line
|

"
--- a/\($file)
+++ b/\($file)
@@ -\($line),0 +\($line + 1) @@
+\($impl)
"
