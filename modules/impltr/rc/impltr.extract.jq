#!/usr/bin/env -S jq --raw-input --slurp --rawfile rgx impltr.rgx -f
# vim: et ts=2 sw=2
# https://regex101.com/r/0OQjpj/2
# error\[E0046]:.*\R  --> (?'file'[^:]+):(?'line'[^:]+):(?'column'.*)\R([\s\d]+\|.*\R)*[\s*=]*help:[^:]*:\s*`(?'fix'[^`]+)`

.
| "" as $flags
| {
    $rgx,
    match: match($rgx; "xmg")
  }
| debug
| .match.captures
| map(.
  |. as { $string, $name }
  | if $name then { ($name): $string } else empty end
  )
| add
|.
