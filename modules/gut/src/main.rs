extern crate list;
extern crate x;
use {
    std::{collections::HashMap, fmt, fs, io},
    x::Error,
};

macro_rules! debug {
    ($($t:tt)*) => {
        // eprintln!($($t)*)
    };
}
pub type R<T> = Result<T, Error>;
fn main() -> R<()> {
    let opts = Opts::from_iter(std::env::args())?;
    //let env: HashMap<String, String> = std::env::vars().collect();
    let config = Config::default();
    let command = Command::find(&opts.command_name)?;
    let stdin = Box::new(io::stdin());
    let stdout = Box::new(io::stdout());
    let stderr = Box::new(io::stderr());
    let mut ctx = CommandCtx {
        config,
        opts,
        stdin,
        stdout,
        stderr,
    };
    command.exec(&mut ctx)?;
    Ok(())
}

pub struct CommandCtx {
    pub stdin: Box<dyn io::Read>,
    pub stdout: Box<dyn io::Write>,
    pub stderr: Box<dyn io::Write>,
    pub config: Config,
    pub opts: Opts,
}
pub struct Command {
    exec: fn(&mut CommandCtx) -> R<()>,
}
impl Command {
    pub fn find<N>(name: N) -> R<Self>
    where
        N: for<'r> PartialEq<&'r str> + fmt::Display,
    {
        if let Some(&(_, exec)) = commands::COMMANDS.into_iter().find(|&(n, _)| name == n) {
            Ok(Self::new(exec))
        } else {
            Err(format!("Command not found: {}", name).into())
        }
    }
    pub fn new(exec: fn(&mut CommandCtx) -> R<()>) -> Self {
        Self { exec }
    }
    pub fn exec(&self, ctx: &mut CommandCtx) -> R<()> {
        (self.exec)(ctx)
    }

    pub fn help(ctx: &mut CommandCtx) -> R<()> {
        for (cmd_name, _) in commands::COMMANDS {
            writeln!(ctx.stdout, "- {}", cmd_name)?;
        }
        Ok(())
    }
    pub fn list(ctx: &mut CommandCtx) -> R<()> {
        let gut = Gut::parse(x::fakebug::Bug(&mut ctx.stdin))?;
        for (name, _) in &gut.entries {
            writeln!(ctx.stdout, "- {}", name)?;
        }
        Ok(())
    }
    pub fn makefile(ctx: &mut CommandCtx) -> R<()> {
        commands::makefile::makefile(ctx)
    }
}
mod commands {
    pub mod makefile;
    use super::{Command, CommandCtx, R};
    pub const COMMANDS: &[(&str, fn(&mut CommandCtx) -> R<()>)] = &[
        ("list", Command::list),
        ("makefile", Command::makefile),
        ("help", Command::help),
    ];
}

#[derive(Default, Debug)]
struct GutEntry {
    local: String,
    url: String,
    sha512: String,
}
#[derive(Debug)]
pub struct Gut {
    entries: HashMap<String, GutEntry>,
}
impl Gut {
    pub fn _from_config(config: &Config) -> R<Self> {
        let file = fs::File::open(&config.gutfile)?;
        let file = io::BufReader::new(file);
        Self::parse(file)
    }
    pub fn parse<I: io::Read + fmt::Debug>(inp: I) -> R<Self> {
        use list::{
            jq,
            jq::{
                filter::Filter,
                json,
                stream::{Next, Poll, View},
            },
            util::log0,
        };

        fn string_val(item: &json::Elem<'_>, val: &mut String) -> R<()> {
            match item {
                json::Elem {
                    value: json::Val::Str(s),
                    ..
                } => {
                    val.replace_range(.., s);
                    Ok(())
                }
                json::Elem { path, .. } => Err(format!("not a string value at {:?}", path).into()),
            }
        }

        let void = log0::sink::Sink::Void;
        let name_flt = jq!(0?.name);
        let url_flt = jq!(0?.url);
        let sha512_flt = jq!(0?.sha512);
        let local_flt = jq!(0?.local);

        let mut parser = json::Parser::new(inp, void);

        let mut entry_name = String::default();
        let mut entry = GutEntry::default();
        let mut current_idx: usize = 0;
        let mut result = Self {
            entries: Default::default(),
        };
        loop {
            match Next::next(parser)? {
                Poll::Yield(p) => {
                    parser = p;
                    let item = parser.view_as();

                    debug!("{:?}", item.path);

                    let path_len = item.path.len();
                    if path_len > 1 && item.path[1].idx != current_idx || path_len <= 1 {
                        result.entries.insert(entry_name, entry);

                        current_idx = if path_len > 1 { item.path[1].idx } else { 0 };

                        entry_name = String::new();
                        entry = Default::default();
                    }

                    let accept = |f: &dyn Filter| f.accept(&parser, &item, void);
                    if accept(&name_flt) {
                        string_val(&item, &mut entry_name)?;
                    }
                    if accept(&local_flt) {
                        string_val(&item, &mut entry.local)?;
                    }
                    if accept(&sha512_flt) {
                        string_val(&item, &mut entry.sha512)?;
                    }
                    if accept(&url_flt) {
                        string_val(&item, &mut entry.url)?;
                    }
                }
                Poll::Cont(p) => {
                    parser = p;
                }
                Poll::Done => {
                    break;
                }
            }
        }
        Ok(result)
    }
}

#[derive(Debug, Default)]
pub struct Config {
    gutfile: String,
    cache: String,
}
impl Config {
    pub fn default() -> Self {
        Self {
            gutfile: "gut.json".to_owned(),
            cache: "_.gut".to_owned(),
        }
    }
}
#[derive(Debug, Default)]
pub struct Opts {
    pub exec_name: String,
    pub command_name: String,
    pub args: Vec<String>,
}
impl Opts {
    pub fn from_iter<I>(iter: I) -> R<Self>
    where
        I: IntoIterator,
        I::Item: AsRef<str>,
    {
        let mut iter = iter.into_iter();
        let mut next = |name: &str| {
            iter.next()
                .map(Ok)
                .unwrap_or_else(|| Err(format!("{} missing", name)))
        };
        let mut next_string = |name: &str| next(name).map(|s| s.as_ref().to_owned());
        let exec_name: String = next_string("exec_name")?;
        let command_name: String = next_string("command_name")?;
        let args = iter.map(|s| s.as_ref().to_owned()).collect();
        Ok(Self {
            exec_name,
            command_name,
            args,
        })
    }
}

pub mod errors {
    use super::*;
    pub type FreeForAll = String;
    pub type FreeForAll2 = &'static str;
    pub type Io = io::Error;
    pub type JsonParse = list::jq::auto::Error;
}
Error!(FreeForAll, FreeForAll2, Io, JsonParse);
impl fmt::Debug for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Error::FreeForAll(msg) => write!(f, "{}", msg),
            Error::FreeForAll2(msg) => write!(f, "{}", msg),
            Error::Io(err) => write!(f, "Io: {:?}", err),
            Error::JsonParse(err) => write!(f, "{:?}", err),
        }
    }
}
