# vim: et ts=2 sw=2
def module_entry_to_cargo_toml:. as {
  key: $name,
  value: {
    $version,
    $deps,
    $crate_deps,
    $dev_deps,
    $dev_crate_deps,
    $lib,
    $features,
    $package
  }
}
| [
    "[package]",
    "name = \"\($name)\"",
    "version = \"\($version // "0.0.0")\"",
    "edition = \"2018\""
  ] +

  if $lib then [
    "",
    ""
  ] else [] end +

  if $deps then [
    "",
    ""
  ] else [] end +

  if $dev_deps then [
    "",
    ""
  ] else [] end +

  if $crate_deps then [
    "",
    ""
  ] else [] end +

  if $dev_crate_deps then [
    "",
    ""
  ] else [] end +

  if $features then [
    "",
    ""
  ] else [] end +

  [] | join("\n")
| {$name, value: .}
|.;

def _:.
| .modules
| to_entries | map(module_entry_to_cargo_toml) | from_entries
|.;
