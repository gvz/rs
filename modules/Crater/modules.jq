# vim: et ts=2 sw=2

def _: {
  __sample: {
    version: "0.0.0",
    deps: [],
    crate_deps: [],
    dev_deps: [],
    dev_crate_deps: [],
    lib: {},
    features: {},
    package: "sample"
  },
  portal: {
    crate_deps: ["wasm-bindgen", "console_error_panic_hook", "wee_alloc"],
    features: {
      default: ["webgl", "wasm", "console_error_panic_hook", "wee_alloc"],
      webgl: [],
      wasm: []
    },
    lib: {
      crate_type: ["cdylib", "rlib"]
    }
  },
  gut: { deps: ["x"], crate_deps: ["husk"] },
  x: { version: "0.0.1", crate_deps: ["log"] },
  deckard: { deps: ["x"], features: { default: [], "cfg-cargo-depl": [] } },
  crater: { deps: ["x"], crate_deps: ["husk"] },
  z01: { deps: ["x"] },
  impltr: { deps: ["x"], crate_deps: ["husK"] },
  gust: { deps: ["x"], crate_deps: ["husK"] }
};
