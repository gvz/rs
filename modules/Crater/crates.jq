# vim: et ts=2 sw=2
def _: {
  gvz_list: {
      version: "0.0.1",
      package: "list",
      source: {
        git: "https://gitlab.com/gvz/list",
        commit: "22039"
      }
  },

  gvz_x: {
    version: "0.0.1",
    source: {
      git: "https://gitlab.com/gvz/rs"
    }
  },

  wasm_bindgen: {
    version: "0.2",
    features: ["enable-interning"]
  },

  console_error_panic_hook: {
    version: "0.1",
    optional: true
  },

  wee_alloc: {
    version: "0.4",
    optional: true
  },

  web_sys: {
    version: "= -.3.37",
    features: [
      "console",
      "Document",
      "Element",
      "HtmlElement",
      "Node",
      "Window",
      "HtmlCanvasElement",
      "WebGlActiveInfo",
      "WebGlProgram",
      "WebGlRenderingContext",
      "WebGlShader",
      "WebGlBuffer",
      "WebGlUniformLocation",
      "HtmlTextAreaElement",
      "KeyboardEvent"
    ]
  },

  js_sys: {
    version: "= 0.3"
  },

  log: {
    version: "0.4.0",
    source: {
      git: "https://github.com/rust-lang/log",
      commit: "adssdasd"
    },
    features: ["std"]
  },

  __placeholder: null
};
