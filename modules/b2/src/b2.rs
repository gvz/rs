// void uploadFiles(Queue<UploadInfo> queue) {
//    // Initially, we don't have an upload URL and authorization token
//    UrlAndAuthToken urlAndAuthToken = null;
//
//    // Keep looping and uploading files forever
//    while (true) {
//        // Get the info on the next file to upload
//        UploadInfo uploadInfo = queue.take();
//
//        // Try several times to upload the file.  It's normal
//        // for uploads to fail if the target storage pod is
//        // too busy.  It's also normal (but infrequent) to get
//        // a 429 Too Many Requests if you are uploading a LOT
//        // of files.
//
//        boolean succeeded = false;
//        for (int i = 0; i < 5 && !succeeded; i++) {
//
//            // Get a new upload URL and auth token, if needed.
//            if (urlAndAuthToken == null) {
//                B2Request getUrlRequest = makeGetUploadUrlRequest();
//                B2Response getUrlResponse = callB2WithBackOff(request);
//                int status = response.status;
//                if (status != 200 /*OK*/) {
//                     reportFailure(uploadInfo, response);
//                     return;
//                }
//                urlAndAuthToken = response.getUrlAndAuthToken();
//            }
//
//            // Upload the file.  When calling upload, don't use
//            // back-off.  If there's any problem, we want to go
//            // around the loop again and get another upload URL.
//            B2Request uploadRequest = makeUploadRequest(uploadInfo);
//            B2Response response = callHttpService(uploadRequest)
//            int status = response.status;
//            if (status == 200 /*OK*/) {
//                reportSuccess(uploadInfo);
//                succeeded = true;
//                break;
//            }
//            else if (response.isFailureToConnect()) {
//                // Try connecting somewhere else next time.
//                urlAndAuthToken = null;
//            }
//            else if (response.isBrokenPipe()) {
//                // Could not send entire file.  Try connecting somewhere else next time.
//                // If upload caps are exceeded, the next call to get an upload URL will
//                // respond with a useful error message.
//                urlAndAuthToken = null;
//            }
//            else if (status == 401 /* Unauthorized */ &&
//                       (response.status_code.equals("expired_auth_token") || response.status_code.equals("bad_auth_token")) {
//                // Upload auth token has expired.  Time for a new one.
//                urlAndAuthToken = null;
//            }
//            else if (status == 408 /* Request Timeout */) {
//                // Retry and hope the upload goes faster this time
//                exponentialBackOff();
//            }
//            else if (status == 429 /* Too Many Requests */) {
//                // We are making too many requests
//                exponentialBackOff();
//            }
//            else {
//                // Something else went wrong.  Give up.
//                reportFailure(uploadInfo, response);
//                return;
//            }
//        }
//
//        if (!succeeded) {
//            reportFailure(uploadInfo, response);
//            return;
//        }
//    }
//}
//
//B2Response callB2WithBackOff(B2Request request) {
//    int delaySeconds = 1;
//    int maxDelay = 64;
//    while (true) {
//        B2Response response = callHttpService(request);
//        int status = response.status;
//        if (status == 429 /*Too Many Requests*/) {
//            sleepSeconds(response.getHeader('Retry-After'));
//            delaySeconds = 1.0; // reset 503 back-off
//        }
//        else if (status == 503 /*Service Unavailable*/) {
//            if (maxDelay < delaySeconds) {
//                // give up -- delay is too long
//                return response
//            }
//            sleepSeconds(delaySeconds);
//            delaySeconds = delaySeconds * 2;
//        }
//        else {
//            return response;
//        }
//    }
//}
use super::{process, Result};

pub(super) struct B2 {
    curl: process::Command,
}
pub(super) struct Connection {
    curl: process::Child,
}

//TODO: obliterate &mut self
impl B2 {
    pub fn new() -> Self {
        let mut curl = process::Command::new("curl");
        curl.args(&["-h"]);
        Self { curl }
    }
    pub fn connect(&mut self) -> Result<Connection> {
        let curl = self.curl.spawn()?;
        Ok(Connection { curl })
    }
    pub fn auth(&mut self) -> Result<()> {
        todo!()
    }
}
impl Connection {
    //      open_remote.S: AsRef<str>(path: S)  # path.as_ref()...  path.as_ref
    // <=>  open_remote.S: AsRef<str>(S)        # S.as_ref()...     S.as_ref
    // <=>  open_remote.AsRef<str>              # as_ref()...       as_ref
    pub fn open_remote<S: AsRef<str>>(self, path: S) -> Result<UploadStream> {
        Ok(UploadStream(self))
    }
}
pub struct UploadStream(Connection);
