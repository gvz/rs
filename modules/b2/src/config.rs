use {
    super::{
        Result,
        Poll,
        poll,
    },
};
pub(super) fn load() -> Poll<Config> {
    let commands = Command::UploadFile {
        source: "test.txt".to_owned(),
        target: "b2://Bob/test.txt".to_owned(),
    };
    let commands = vec![commands].into_iter().map(Ok);
    let command_source = Box::new(commands);
    let r = Config {
        command_source
    };
    poll::ok(r)
}
pub(super) struct Config {
    pub command_source: Box<dyn Iterator<Item = Result<Command>>>,
}
pub(super) enum Command {
    UploadFile { source: String, target: String }
}
