use super::Result;

pub(super) type Poll<T> = Result<Option<T>>;

pub(super) fn ok<T>(t: T) -> Poll<T> {
    Ok(Some(t))
}
pub(super) fn done<T>() -> Poll<T> {
    Ok(None)
}
