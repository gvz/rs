use super::{io, result};

macro_rules! Error {
    ($($var:ident = $s:ty),*) => {
        #[derive(Debug)]
        pub(super) enum Error {
            $( $var($s), )*
        }
        $(
            impl From<$s> for Error {
                fn from(e: $s) -> Self { Self::$var(e) }
            }
        )*
    };
}
Error! {
    Io = io::Error
}
pub(super) type Result<T> = result::Result<T, Error>;
