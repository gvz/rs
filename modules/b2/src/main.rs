use {
    self::{
        error::{Error, Result},
        poll::Poll,
        engine::Engine,
    },
    std::{collections::VecDeque as Deq, fs, io, path::Path, process, result},
};
mod b2;
mod config;
mod error;
mod poll;
mod engine;

fn app_main() -> Result<()> {
    if let Some(mut config) = config::load()? {
        let mut engine = Engine::new();
        while let Some(command) = config.command_source.next().transpose()? {
            engine.dispatch(command)?;
        }
    }

    Ok(())
}
fn main() {
    match app_main() {
        Ok(_) => {}
        Err(err) => panic!(format!("{:?}", err)),
    }
}
