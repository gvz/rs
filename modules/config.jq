# vim: et ts=2 sw=2

import "./Crater/crates" as crates;
import "./Crater/modules" as modules;
import "./Crater/lib" as lib;

{
  crates: crates::_,
  modules: modules::_,
}
| lib::_
